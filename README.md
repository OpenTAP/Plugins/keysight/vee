# OpenTAP VEE Plugin Project
An OpenTAP plugin to support calling UserFunction(s) in VEE programs. The goal of this plugin is for the end-user to minimize the modifications to any of their VEE files in the integration with OpenTAP.

## User Guide
- *The guidance on using the OpenTAP VEE plugin can be found at https://opentap.gitlab.io/Plugins/keysight/vee/*
