﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;
using Moq;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.OpenTapTests
{
    public class RunOnceTestStepTests
    {
        [Test]
        public void RunOnceTestStepTest()
        {
            var mockStep = new Mock<RunOnceTestStep>();
            Assert.DoesNotThrow(() => mockStep.Object.PrePlanRun());
            Assert.DoesNotThrow(() => mockStep.Object.Run());
            Assert.DoesNotThrow(() => mockStep.Object.PostPlanRun());
        }
    }
}
