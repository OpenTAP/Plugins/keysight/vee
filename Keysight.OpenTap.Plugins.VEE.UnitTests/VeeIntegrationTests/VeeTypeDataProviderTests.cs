﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;
using Moq;
using OpenTap;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.VeeIntegrationTests
{
    public class VeeTypeDataProviderTests
    {
        [Test]
        public void VeeTypeDataProviderTest()
        {
            var mockObject = new Mock<VeeTypeDataProvider>();

            Assert.IsNull(mockObject.Object.Types);
            Assert.That(mockObject.Object.Priority > 0);
            Assert.DoesNotThrow(() => mockObject.Object.GetTypeData("test"));
            Assert.DoesNotThrow(() => mockObject.Object.GetTypeData(new object()));
            Assert.DoesNotThrow(() => mockObject.Object.Search());
        }
    }
}
