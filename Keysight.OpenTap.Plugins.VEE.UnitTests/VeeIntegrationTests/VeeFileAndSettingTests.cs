﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;
using Moq;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.VeeIntegrationTests
{
    public class VeeFileTests
    {
        [Test]
        public void VeeFileTest()
        {
            var mockObject = new Mock<VeeFile>();

            Assert.IsNull(mockObject.Object.Name);
            Assert.IsNull(mockObject.Object.FilePath);
            Assert.That(mockObject.Object.IsEnabled == true);
            Assert.That(mockObject.Object.Rules.Count != 0);
        }
    }

    public class VeeSettingsTests
    {
        [Test]
        public void VeeSettingsTest()
        {
            var mockObject = new Mock<VeeSettings>();

            Assert.IsNotNull(mockObject.Object.VeeFiles);
        }
    }

}
