﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;
using Moq;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.VeeIntegrationTests
{
    public class VeeDescriptorTests
    {
        [Test]
        public void VeeDescriptorTest()
        {
            var mockObject = new Mock<VeeDescriptor>();

            Assert.IsNull(mockObject.Object.FunctionName);
            Assert.IsNull(mockObject.Object.Description);
            Assert.That(mockObject.Object.FunctionType == VeeFunctionTypes.UserFunction);
            Assert.IsNull(mockObject.Object.Arguments);
            Assert.IsNull(mockObject.Object.FilePath);
            Assert.IsNull(mockObject.Object.FileName);
        }
    }
}
