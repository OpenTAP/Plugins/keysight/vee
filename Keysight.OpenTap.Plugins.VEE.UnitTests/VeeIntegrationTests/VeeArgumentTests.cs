﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;
using Moq;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.VeeIntegrationTests
{
    public class VeeArgumentTests
    {
        [Test]
        public void VeeArgumentTest()
        {
            var mockObject = new Mock<VeeArgument>();

            Assert.IsNull(mockObject.Object.Name);
            Assert.IsNull(mockObject.Object.Description);
            Assert.That(mockObject.Object.Index == 0);
            Assert.That(mockObject.Object.Input == false);
            Assert.IsNull(mockObject.Object.OpenTapTypeData);
        }
    }
}
