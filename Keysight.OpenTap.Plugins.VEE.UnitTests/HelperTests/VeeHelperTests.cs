﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class VeeHelperTests
    {
        [Test]
        public void GetVeeErrorMessageTest()
        {
            var input = new Exception("test");
            var expectedOutput = "test";
            var actualOutput = VeeHelper.GetVeeErrorMessage(input, string.Empty);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
