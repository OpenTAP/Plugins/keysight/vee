﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class DataTypeHelperTests
    {
        [Test]
        public void GetShapeCaseTest()
        {
            var actualOutput = DataTypeHelper.GetShapeCase(OpenTapDataType.Int16);
            Assert.AreEqual("Case1", actualOutput);
            Assert.AreNotEqual("Case2", actualOutput);
        }

        [Test]
        public void GetDataTypefor2DArrayElementsTest()
        {
            var actualOutput = DataTypeHelper.GetDataTypefor2DArrayElements(OpenTapDataType.Int16);
            Assert.AreEqual(typeof(short), actualOutput);
            Assert.AreNotEqual(typeof(long), actualOutput);
        }

        [Test]
        public void GetValueTypeAndShapeTest()
        {
            OpenTapDataType actualTypeOutput;
            OpenTapDataShape actualShapeOutput;

            DataTypeHelper.GetValueTypeAndShape(string.Empty, string.Empty, "testString", out actualTypeOutput, out actualShapeOutput);

            Assert.AreEqual(OpenTapDataType.Text, actualTypeOutput);
            Assert.AreEqual(OpenTapDataShape.Scalar, actualShapeOutput);
        }
    }
}