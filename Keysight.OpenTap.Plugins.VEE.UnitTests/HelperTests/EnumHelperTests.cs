﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class EnumHelperTests
    {
        public enum TestValue
        {
            abc,
            def
        }

        [Test]
        public void GetValuesTest()
        {
            var actualOutput = EnumHelper.GetValues<TestValue>();
            var expectedOutput = new List<TestValue> { TestValue.abc, TestValue.def };

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void GetDisplayNameTest()
        {
            var actualOutput = "abc";
            var expectedOutput = EnumHelper.GetDisplayName(TestValue.abc);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

    }
}
