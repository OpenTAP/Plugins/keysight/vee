﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class StringCheckHelperTests
    {
        [Test]
        public void StringCheckTest()
        {
            var expectedOutput = true;
            var actualOutput = StringCheckHelper.StringCheck("abc", "abc", StringOperator.Equals);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
