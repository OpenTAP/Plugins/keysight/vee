﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class BoolCheckHelperTests
    {
        [TestCase(true, true)]
        [TestCase(false, false)]
        public void BoolCheckTest(dynamic input, bool expectedOutput)
        {
            bool actualOutput = BoolCheckHelper.BoolCheck(input);
            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
