﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class LimitCheckHelperTests
    {
        int testValue = 5;
        int upperLimit = 10;
        int lowerLimit = 0;
        LimitOperator upperOperator = LimitOperator.GreaterThan;
        LimitOperator lowerOperator = LimitOperator.LessThan;

        [Test]
        public void RangeTest()
        {
            var expectedOutput = true;
            var actualOutput = LimitCheckHelper.Range(testValue, upperLimit, upperOperator, lowerLimit, lowerOperator);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void SingleLimitTest()
        {
            var expectedOutput = true;
            var actualOutput = LimitCheckHelper.SingleLimit(testValue, upperLimit, upperOperator);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void ToleranceTest()
        {
            var expectedOutput = true;
            var actualOutput = LimitCheckHelper.Tolerance(testValue, testValue, testValue, testValue);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void TolerancePercentTest()
        {
            var expectedOutput = true;
            var actualOutput = LimitCheckHelper.TolerancePercent(testValue, testValue, testValue, testValue);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
