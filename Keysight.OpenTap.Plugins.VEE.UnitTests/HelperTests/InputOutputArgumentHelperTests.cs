﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using OpenTap.Plugins.VeeIntegration;

namespace Keysight.OpenTap.Plugins.VEE.UnitTests.HelperTests
{
    public class InputOutputArgumentHelperTests
    {
        [Test]
        public void SetOutputValuesTest()
        {
            object actualPublishOutput;
            object actualDisplayOutput;

            InputOutputArgumentHelper.SetOutputValues("1", "1", out actualPublishOutput, out actualDisplayOutput);

            Assert.AreEqual("1", actualPublishOutput);
            Assert.AreEqual("1", actualDisplayOutput);
        }

        [Test]
        public void GetColumnLengthTest()
        {
            string input = "(1,2,3)";
            var actualOutput = InputOutputArgumentHelper.GetColumnLength(string.Empty, string.Empty, input);

            Assert.AreEqual(3, actualOutput);
        }

        [Test]
        public void CreateCoordArrayTest()
        {
            string input = "(1,2)";
            var actualOutput = InputOutputArgumentHelper.CreateCoordArray(string.Empty, string.Empty, input);

            Assert.AreEqual(new double[] { 1, 2 }, actualOutput);
        }

        [Test]
        public void GetCoordDisplay1DArrayTest()
        {
            short inputAxes = 2;
            var inputArray = new double[] { 1, 2, 3, 4 };
            var actualOutput = InputOutputArgumentHelper.GetCoordDisplay1DArray(inputAxes, inputArray);

            Assert.AreEqual("(1, 2), (3, 4)", actualOutput);
        }

        [Test]
        public void Get2DArrayTest()
        {
            var inputArray = "(1,2),(3,4)";
            var expectedOutput = new int[,] { { 1, 2 }, { 3, 4 } };
            var actualOutput = InputOutputArgumentHelper.Get2DArray(string.Empty, string.Empty, inputArray, typeof(int));

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void GetDisplay2DArrayTest()
        {
            var input = new int[,] { { 1, 2 }, { 3, 4 } };
            var expectedOutput = "(1, 2), (3, 4)";
            var actualOutput = InputOutputArgumentHelper.GetDisplay2DArray(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void GetDisplay1DArrayTest()
        {
            var input = new int[] { 1, 2 };
            var expectedOutput = "1, 2";
            var actualOutput = InputOutputArgumentHelper.GetDisplay1DArray(input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void CheckInputFormatFor1DArrayTest()
        {
            var input = "a,b,c,";
            Assert.Throws(typeof(Exception), () => InputOutputArgumentHelper.CheckInputFormatFor1DArray(string.Empty, string.Empty, input));
        }

        [Test]
        public void CheckInputFormatFor2DArrayTest()
        {
            var input = "(1, 2), (3, 4)";
            var expectedOutput = 2;
            var actualOutput = InputOutputArgumentHelper.CheckInputFormatFor2DArray(string.Empty, string.Empty, input);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
