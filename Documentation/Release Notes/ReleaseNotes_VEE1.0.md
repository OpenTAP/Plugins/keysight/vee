Release Notes - OpenTAP VEE Plugin 1.0
=============

New Features:
-------

- Need to add dropdown in Step setting when input / output is type `Any`; allow the user to select the appropriate datatype [#4](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/4)
- Handle all of the VEE datatypes appropriately as inputs / outputs from VEE UserFunction [#5](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/5)
- Allow user to specify where VEE files are located (plugin settings) [#7](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/7)
- Publish output as results [#8](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/8)
- Map VEE Input/Outputs to TAP Input/Outputs [#9](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/9)
- Set CallableVEE Flag for Debug [#20](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/20)
- Support VEE Sequencer Functionality [#21](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/21)
- Add string verification feature for outputs [#23](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/23)
- Need support for compiled VEE files `*.vxe` [#28](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/28)
- Add `RunOnce` Test Step [#38](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/38)
- Need a way to clean-up VEE environment when loading new testplan [#47](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/47)
- Must publish limits and verdict to result listeners [#48](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/48)

Usability Improvements:
-------

- Throw appropriate error when VEE is not installed [#6](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/6)
- Package as a Plugin [#10](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/10)
- Add FileName into Test Step Group [#15](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/15)
- Add Description of VEE Function [#17](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/17)
- Add Description for VEE Settings [#18](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/18)
- Set up CI pipeline [#24](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/24)
- Move Publish Output to Result Listener from VEE Settings to Test Step Settings [#26](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/26)
- VEE file Search Path dialog should target individual `.vee/.vxe` files and not complete folders [#29](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/29)
- Set default data type for inputs to `Text` instead of `Real64` [#31](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/31)
- In Editor, Type column shows `VeeTestStep` for all VEE test-steps [#36](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/36)
- Remove unsupported data types in Test Step Settings [#39](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/39)
- Re-organize Test Step Settings fields [#42](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/42)
- Remove `Output Check` section in Test Step Settings if UserFunction has no outputs [#43](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/43)
- Upgrade OpenTap Version [#45](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/45)
- Output handling could be simplified [#49](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/49)
- Error log from VEE need more details [#51](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/51)
- Create unit test for vee plugin [#52](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/52)
- Organize Solution Folder [#54](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/54)
- CI pipeline modification [#55](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/55)

Bug Fixes:
-------

- VEE shows `Error 700` when called by CallableVEE [#2](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/2)
- Show Test Step name in Test Plan Panel [#11](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/11)
- Kill excessive VEE Instance [#27](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/27)
- Get Serializer XML warnings when loading a testplan with VEE test steps [#32](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/32)
- Errors in VEE UF's not handled well, VEE process remains [#34](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/34)
- Copy/paste VEE test step can paste wrong test step [#37](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/37)
- Output values added as Test plan column doesn't show up [#40](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/40)
- Assign Output to Input lists available Outputs multiple times [#44](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/44)

Documentation: 
-------

- Dynamically Load and Delete Libraries within the Test Plan [#16](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/16)
- Use `$(GitVersion)` in package.xml [#41](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/41)
- Create documentation for vee plugin [#53](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/53)
- Include help file for Callable VEE API server [#56](https://gitlab.com/OpenTAP/Plugins/keysight/vee/-/issues/56)