// JavaScript source code
module.exports = {
    title: 'OpenTAP VEE Plugin',
    description: 'VEE Integration for OpenTAP',

    themeConfig: {
        repo: 'https://gitlab.com/OpenTAP/Plugins/keysight/vee',
        editLinks: true,
        editLinkText: 'Help improve this page!',
        docsDir: 'Documentation',
        nav: [
            { text: 'OpenTAP', link: 'https://gitlab.com/opentap/opentap' },
            { text: 'OpenTAP Homepage', link: 'https://www.opentap.io' }
        ],
        sidebar: [
            ['/', "Welcome"],
            {
                title: "Using the Plugin",
                children: [
                    ['VEE Plugin Usage Help/AddingVEEFiles.md', 'Adding VEE files to the plugin'],
                    ['VEE Plugin Usage Help/ConfiguringTestSteps.md', 'Configuring TestSteps'],
                    ['VEE Plugin Usage Help/ConfiguringOutputCheck.md', 'Configuring Output Check'],
                    ['VEE Plugin Usage Help/ViewingResult.md', 'Result Viewing'],
                ]
            },
            {
                title: "Limitations",
                children:
                    [
                        ['VEE Plugin Usage Help/VEEError700.md', 'Vee Error 700'],
                        ['VEE Plugin Usage Help/VEELibraryDependencies.md', 'Loading VEE Libraries or Variables of a VEE file in OpenTap']
                    ]
            },
            ['CallableVEE/', 'Callable VEE'],
            {
                title: 'Release Notes',
                children: [
                    ['Release Notes/ReleaseNotes_VEE1.0.md', 'OpenTAP VEE Plugin 1.0']
                ]
            }
        ]
    },
    dest: '../public',
    base: '/Plugins/keysight/vee/'
}