﻿# Configuring Output check for output values

- Once the input parameters are configured, there is an option to configure output parameter checking in step settings.(Note that for multiple output steps, the output checking is only done for the first output step as done by VEE Pro)

- Please choose the correct output check based on the output parameter type. There are three output checking types:
	- *Limit Check* for integer output type
	- *String Check* for text output type
	- *Boolean Check* for boolean output type
![](./Images/OutputCheckingTypes.png)

- The step verdict will Pass/Fail after run based on the output check.
![](./Images/OutputCheckPass.png)
![](./Images/OutputCheckFail.png)

- If the wrong output check is chosen, there will be error prompt at the log window to choose the right output check
![](./Images/OutputCheckError.png)