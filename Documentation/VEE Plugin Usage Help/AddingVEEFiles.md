# Adding VEE File Path to OpenTap for conversion

- Navigate to Settings > VEE on the top menu bar of OpenTap Editor
![](./Images/NavigateToVEESttings.png)

- Click on edit in VEE File(s). Mention the VEE File path and close the window and then click OK.(Please note that each VEE file name is unique)
![](./Images/MentionTheFilePath.png)
- Restart OpenTap Editor for the conversion of the VEE file to OpenTap test steps.

- To view the test steps, go to View > Panels > TestSteps on the top menu bar. The converted teststeps will be categorised under VEE - (VEE File Name) - (UserFunctions Name)
![](./Images/ConvertedTestSteps.png)