# Loading VEE Library dependencies/variables in VEE file

For the OpenTAP VEE plugin to be functional, you will need to modify the VEE files.

1.	In the VEE files, the UserFunction(s) may depend on the VEE variables and/or external VEE libraries. 
![](./Images/UserFunctionSample.png)


2.	You will need to create an UserFunction named **LoadDependency**.
- It shall initialize the VEE variable(s).
- It shall import the external VEE library(s). 
![](./Images/LoadDependency.png)

- You shall save the path of external VEE file as an absolute path.
![](./Images/ImportLibraryDialog.png)

3.	You will also need to create an UserFunction named **UnloadDependency** to unload and clear the dependencies.
![](./Images/UnloadDependency.png)

4.	In OpenTAP, you shall run the test step “LoadDependency” **before** any other test steps from the same VEE file to ensure all dependencies have been loaded.

5.	In OpenTAP, you shall run the test step “UnloadDependency” **after** any other test steps from the same VEE file to ensure all dependencies have been cleared up.
![](./Images/OpenTapExample.png)