# VEE Error 700

1. When you launch the OpenTAP, VEE may throw an error as shown below. Please follow the steps below to fix this error.

![](./Images/VeeError700.png)

- Go to the *OpenTAP installation folder* >> *Packages* >> *VEE*. Directory Path: *%TAP_PATH%\Packages\VEE*

- Run the **RegistryCheck.exe** in administrator mode.

- Exit the console application.

- Restart the OpenTAP.

- Done.
