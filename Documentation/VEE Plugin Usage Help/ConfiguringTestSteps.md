# Adding and configuring test steps

- Click on Add to add the teststeps to the testplan window.
![](./Images/AddingTheTestSteps.png)

- Once the teststep is added, configure the step from the step settings window.

- The step settings contain input and output section. Every input value contains three sub categories namely value, datatype and datashape.
![](./Images/StepSettings.png)

- Select the datatype from the dropdown list of supported types.
![](./Images/StepSettingsDataTypeConfiguration.png)

- Select the datashape from the dropdown list of supported shapes.
![](./Images/StepSettingsDataShapeConfiguration.png)

- Some VEE User Functions contain inputs with defined datatype and datashape. Such test steps in opentap will already contain the configured datatype and datashape. In such cases, only the input value needs to be entered.
![](./Images/StrictDataTypeAndShape.png)

- If you want to configure output checking, refer to [Configuring Output check](ConfiguringOutputCheck.md) 

- Once all the teststeps are added and step settings are configured, run the testplan.
