﻿# Viewing output value in step settings and result listener

- Once the test plan is run, the test step outputs can be viewed at the output section of step settings.
![](./Images/OutputViewing.png)

- If you are unable to see the output, one possibility is the selection of wrong datatype and/or datashape. Please check for warning prompt in the log window and select the appropriate type and shape.
![](./Images/OutputViewingWarning.png)

- If you want the result to be published to result listener, please enable publish to result listener on the common section of test steps.
![](./Images/PublishToResultListener.png)