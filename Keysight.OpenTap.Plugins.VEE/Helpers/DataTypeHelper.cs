﻿using CallableVEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for data conversion between VEE test step and VEE user function.
    /// </summary>
    public static class DataTypeHelper
    {
        internal static KeyValuePair<Type, object[]> GetMatchingTypePair(OpenTapDataType dataType, OpenTapDataShape dataShape)
        {
            return DotNetVeeTypeMatchingTable.FirstOrDefault(x => x.Value.Contains($"{dataType}:{dataShape}"));
        }

        internal static Dictionary<Type, object[]> GetMatchingTypePair(OpenTapDataShape dataShape)
        {
            return DotNetVeeTypeMatchingTable.Where(x => x.Value.Any(y => y.ToString().Contains(dataShape.ToString()))).ToDictionary(z => z.Key, z => z.Value);
        }

        internal static Dictionary<Type, object[]> GetMatchingTypePair(OpenTapDataType dataType)
        {
            return DotNetVeeTypeMatchingTable.Where(x => x.Value.Any(y => y.ToString().Contains(dataType.ToString()))).ToDictionary(z => z.Key, z => z.Value);
        }

        internal static readonly Dictionary<Type, object[]> DotNetVeeTypeMatchingTable = new Dictionary<Type, object[]>
        {
            {typeof(short), new object[]{ $"{OpenTapDataType.Int16}:{OpenTapDataShape.Scalar}"}},
            {typeof(short[]), new object[]{ $"{OpenTapDataType.Int16}:{OpenTapDataShape.Array1D}"}},
            {typeof(int), new object[]{ $"{OpenTapDataType.Int32}:{OpenTapDataShape.Scalar}"}},
            {typeof(int[]), new object[]{ $"{OpenTapDataType.Int32}:{OpenTapDataShape.Array1D}"}},
            {typeof(long), new object[]{ $"{OpenTapDataType.Int64}:{OpenTapDataShape.Scalar}"}},
            {typeof(long[]), new object[]{ $"{OpenTapDataType.Int64}:{OpenTapDataShape.Array1D}"}},
            {typeof(float), new object[]{ $"{OpenTapDataType.Real32}:{OpenTapDataShape.Scalar}"}},
            {typeof(float[]), new object[]{ $"{OpenTapDataType.Real32}:{OpenTapDataShape.Array1D}"}},
            {typeof(double), new object[]{ $"{OpenTapDataType.Real64}:{OpenTapDataShape.Scalar}"}},
            {typeof(byte), new object[]{ $"{OpenTapDataType.UInt8}:{OpenTapDataShape.Scalar}"}},
            {typeof(byte[]), new object[]{ $"{OpenTapDataType.UInt8}:{OpenTapDataShape.Array1D}"}},
            {typeof(ushort), new object[]{ $"{OpenTapDataType.UInt16}:{OpenTapDataShape.Scalar}"}},
            {typeof(ushort[]), new object[]{ $"{OpenTapDataType.UInt16}:{OpenTapDataShape.Array1D}"}},
            {typeof(bool), new object[]{ $"{OpenTapDataType.Boolean}:{OpenTapDataShape.Scalar}"}},
            {typeof(double[]), new object[]
            {
                $"{OpenTapDataType.Real64}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Coord}:{OpenTapDataShape.Scalar}",
                $"{OpenTapDataType.PComplex}:{OpenTapDataShape.Scalar}",
                $"{OpenTapDataType.Complex}:{OpenTapDataShape.Scalar}",
                $"{OpenTapDataType.Waveform}:{OpenTapDataShape.Array1D}"}
            },
            {typeof(string), new object[]
            {
                $"{OpenTapDataType.Text}:{OpenTapDataShape.Scalar}",
                $"{OpenTapDataType.Boolean}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Text}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Coord}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.PComplex}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Complex}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Spectrum}:{OpenTapDataShape.Array1D}",
                $"{OpenTapDataType.Int16}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Int32}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Int64}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Real32}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Real64}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.UInt8}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.UInt16}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Boolean}:{OpenTapDataShape.Array2D}",
                $"{OpenTapDataType.Text}:{OpenTapDataShape.Array2D}"}
            }
        };

        internal static readonly Dictionary<string, Tuple<object[], object[]>> CaseSupportedTypes = new Dictionary<string, Tuple<object[], object[]>>()
        {
            {"Case1",
                Tuple.Create(new object[] { OpenTapDataType.Boolean, OpenTapDataType.Int16, OpenTapDataType.Int32, OpenTapDataType.Int64, OpenTapDataType.Real32, OpenTapDataType.Real64, OpenTapDataType.Text, OpenTapDataType.UInt16, OpenTapDataType.UInt8 }, new object[] {OpenTapDataShape.Scalar,OpenTapDataShape.Array1D, OpenTapDataShape.Array2D })},
            {"Case2",
                Tuple.Create(new object[] { OpenTapDataType.Waveform, OpenTapDataType.Spectrum },new object[] {OpenTapDataShape.Array1D})},
            {"Case3",
                Tuple.Create(new object[] { OpenTapDataType.Complex, OpenTapDataType.PComplex, OpenTapDataType.Coord }, new object[] {OpenTapDataShape.Scalar, OpenTapDataShape.Array1D})}
        };

        public static string GetShapeCase(OpenTapDataType dataType)
        {
            string shapeCase = "";
            foreach (var KeyValuePair in DataTypeHelper.CaseSupportedTypes)
            {
                if (KeyValuePair.Value.Item1.Contains(dataType))
                {
                    shapeCase = KeyValuePair.Key;
                    break;
                }
            }
            return shapeCase;
        }

        internal static OpenTapDataType GetOpenTapDataType(VeeDataType veeDataType)
        {
            switch (veeDataType)
            {
                case VeeDataType.veeTypeInt16:
                    return OpenTapDataType.Int16;
                case VeeDataType.veeTypeInt32:
                    return OpenTapDataType.Int32;
                case VeeDataType.veeTypeReal32:
                    return OpenTapDataType.Real32;
                case VeeDataType.veeTypeReal64:
                    return OpenTapDataType.Real64;
                case VeeDataType.veeTypeComplex:
                    return OpenTapDataType.Complex;
                case VeeDataType.veeTypePComplex:
                    return OpenTapDataType.PComplex;
                case VeeDataType.veeTypeText:
                    return OpenTapDataType.Text;
                case VeeDataType.veeTypeCoord:
                    return OpenTapDataType.Coord;
                case VeeDataType.veeTypeWaveform:
                    return OpenTapDataType.Waveform;
                case VeeDataType.veeTypeSpectrum:
                    return OpenTapDataType.Spectrum;
                case VeeDataType.veeTypeUInt8:
                    return OpenTapDataType.UInt8;
                case (VeeDataType)19:
                    return OpenTapDataType.Int64;
                case (VeeDataType)20:
                    return OpenTapDataType.Boolean;
                case (VeeDataType)21:
                    return OpenTapDataType.UInt16;
                case VeeDataType.veeTypeAny:
                case VeeDataType.veeTypeNil:
                    return OpenTapDataType.Any;
                default:
                    return OpenTapDataType.NotSupported;
            }
        }

        internal static OpenTapDataShape GetOpenTapDataShape(VeeDataShape veeDataShape)
        {
            switch (veeDataShape)
            {
                case VeeDataShape.veeShapeScalar:
                    return OpenTapDataShape.Scalar;
                case VeeDataShape.veeShapeArray1D:
                    return OpenTapDataShape.Array1D;
                case VeeDataShape.veeShapeArray2D:
                    return OpenTapDataShape.Array2D;
                case VeeDataShape.veeShapeAny:
                    return OpenTapDataShape.Any;
                default:
                    return OpenTapDataShape.NotSupported;
            }
        }

        public static Type GetDataTypefor2DArrayElements(OpenTapDataType openTapDataType)
        {
            switch (openTapDataType)
            {
                case OpenTapDataType.Int16:
                    return typeof(short);
                case OpenTapDataType.Int32:
                    return typeof(int);
                case OpenTapDataType.Int64:
                    return typeof(long);
                case OpenTapDataType.Real32:
                    return typeof(float);
                case OpenTapDataType.Boolean:
                    return typeof(bool);
                case OpenTapDataType.Text:
                    return typeof(string);
                case OpenTapDataType.UInt8:
                    return typeof(byte);
                case OpenTapDataType.UInt16:
                    return typeof(ushort);
                default:
                    return typeof(double);
            }
        }

        public static void GetValueTypeAndShape(string variableName, string stepName, object value, out OpenTapDataType dataType, out OpenTapDataShape dataShape)
        {
            var valueType = value.GetType();
            var typeName = valueType.Name;

            if (typeName.Contains("__ComObject"))
            {
                dataShape = GetOpenTapDataShape(GetSpecialOutputVeeDataShape(value));
            }
            else if (valueType.IsArray)
            {
                int arrayDimension = typeName.Count(c => c == ',');

                switch (arrayDimension)
                {
                    case 0:
                        dataShape = OpenTapDataShape.Array1D;
                        break;
                    case 1:
                        dataShape = OpenTapDataShape.Array2D;
                        break;
                    default:
                        throw new Exception($"{stepName}.{variableName}: Output datashape is not supported in OpenTap.");
                }
                var arrayBracketIndex = valueType.Name.IndexOf('[');
                typeName = typeName.Substring(0, arrayBracketIndex);
            }
            else
            {
                dataShape = OpenTapDataShape.Scalar;
            }

            switch (typeName)
            {
                case "Int16":
                    dataType = OpenTapDataType.Int16;
                    break;
                case "Int32":
                    dataType = OpenTapDataType.Int32;
                    break;
                case "Int64":
                    dataType = OpenTapDataType.Int64;
                    break;
                case "Single":
                    dataType = OpenTapDataType.Real32;
                    break;
                case "Double":
                    dataType = OpenTapDataType.Real64;
                    break;
                case "String":
                    dataType = OpenTapDataType.Text;
                    break;
                case "Byte":
                    dataType = OpenTapDataType.UInt8;
                    break;
                case "UInt16":
                    dataType = OpenTapDataType.UInt16;
                    break;
                case "Boolean":
                    dataType = OpenTapDataType.Boolean;
                    break;
                case "__ComObject":
                    dataType = GetOpenTapDataType(GetSpecialOutputVeeDataType(value));
                    break;
                default:
                    throw new Exception($"{stepName}.{variableName}: Output datatype is not supported in OpenTap.");
            }

        }

        internal static VeeDataType GetSpecialOutputVeeDataType(object value)
        {
            VeeDataContainer outputValue = (VeeDataContainer)value;
            return outputValue.DataType;
        }
        internal static VeeDataShape GetSpecialOutputVeeDataShape(object value)
        {
            VeeDataContainer outputValue = (VeeDataContainer)value;
            return outputValue.DataShape;
        }

    }

}
