﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for enumeration
    /// </summary>
    public static class EnumHelper
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }

        public static string GetDisplayName<T>(T value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var displayAttribute = fieldInfo.GetCustomAttribute<DisplayAttribute>();
            return displayAttribute == null ? value.ToString() : displayAttribute.Name;
        }
    }
}
