﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for boolean limit check.
    /// </summary>
    public static class BoolCheckHelper
    {
        public static bool BoolCheck(dynamic testValue)
        {
            var valueType = testValue?.GetType();
            string valueTypeName = valueType?.Name;

            if (valueTypeName == null || !valueTypeName.Contains("Boolean"))
            {
                // null or wrong data type
                return false;
            }
            else if (valueType.IsArray)
            {
                // array
                foreach (var value in testValue)
                {
                    if (!value)
                        return false;
                }
                return true;
            }
            else
            {
                return testValue;
            }
        }
    }
}
