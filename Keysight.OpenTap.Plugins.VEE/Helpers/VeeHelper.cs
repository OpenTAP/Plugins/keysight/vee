﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    public static class VeeHelper
    {
        public static string GetVeeErrorMessage(Exception ex, string veeFileName)
        {
            if (ex == null)
                return string.Empty;
            else if (string.Compare(ex.Source, "VEE") != 0)
                return ex.Message;
            else
                return "Error: " + ex.Message.Replace("\n", ". ") + $". VEE File Name: {veeFileName}.";
        }
    }
}
