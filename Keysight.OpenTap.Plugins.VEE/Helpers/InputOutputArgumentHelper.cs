﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for data conversion between string and array.
    /// </summary>
    public static class InputOutputArgumentHelper
    {
        public static void SetOutputValues(object publishData, object displayData, out object publishValue, out object displayValue)
        {
            publishValue = publishData;
            displayValue = displayData;
        }

        public static short GetColumnLength(string variableName, string stepName, object data)
        {
            try
            {
                return (short)data.ToString().Substring(data.ToString().IndexOf('(') + 1, data.ToString().IndexOf(')') - 1).Split(',').Length;
            }
            catch (Exception)
            {
                throw new Exception($"{stepName}.{variableName}: Input not entered in the right format.");
            }
        }

        public static double[] CreateCoordArray(string variableName, string stepName, object data)
        {
            CheckInputFormatFor2DArray(variableName, stepName, data);
            string[] temp = data.ToString().Split(',');
            List<double> coordArrayValues = new List<double>();
            foreach (var value in temp)
            {
                coordArrayValues.Add(Convert.ToDouble(value.Trim('(').Trim(')')));
            }
            return coordArrayValues.ToArray();
        }
        public static string GetCoordDisplay1DArray(object axes, object dataCoord1DArray)
        {
            double[] coordArray = (double[])dataCoord1DArray;
            short coordDim = (short)axes;
            List<string> displayArray = new List<string>();
            List<object> coordNumber = new List<object>();
            for (int i = 0; i < coordArray.Length; i++)
            {
                coordNumber.Add(coordArray[i]);
                if ((i + 1) % coordDim == 0)
                {
                    string element = String.Join(", ", coordNumber);
                    displayArray.Add("(" + element + ")");
                    coordNumber.Clear();
                }
            }
            return String.Join(", ", displayArray);
        }

        public static object Get2DArray(string testStepName, string variableName, dynamic dataArray, Type type, int columnLength = 0)
        {
            dynamic temp = 0;
            if (columnLength == 0)
            {
                columnLength = CheckInputFormatFor2DArray(variableName, testStepName, dataArray);
                temp = dataArray.ToString().Split(',');
            }
            else
            {
                temp = dataArray;
            }

            int ind = 0;
            try
            {
                if (temp.Length % columnLength == 0)
                {
                    object[,] array = new object[(temp.Length / columnLength), columnLength];

                    for (var i = 0; i < array.GetLength(0); i++)
                    {
                        for (var j = 0; j < array.GetLength(1); j++)
                        {
                            string element = (type == typeof(string)) ? temp[ind] : temp[ind].ToString().Replace(" ", "").Trim('(').Trim(')');
                            array[i, j] = Convert.ChangeType(element.Trim('(').Trim(')'), type);
                            ind++;
                        }
                    }
                    return array;
                }
                else
                {
                    throw new Exception($"{testStepName}.{variableName}: Input not entered in the right format.");
                }
            }
            catch (Exception)
            {
                throw new Exception($"{testStepName}.{variableName}: Input not entered in the right format.");
            }
        }

        public static string GetDisplay2DArray(dynamic data2DArray)
        {
            List<string> displayArray = new List<string>();
            for (int i = 0; i < data2DArray.GetLength(0); i++)
            {
                List<object> arrayElements = new List<object>();
                for (int j = 0; j < data2DArray.GetLength(1); j++)
                {
                    arrayElements.Add(data2DArray[i, j]);
                }
                string element = String.Join(", ", arrayElements);
                displayArray.Add("(" + element + ")");
            }
            return String.Join(", ", displayArray);
        }

        public static string GetDisplay1DArray(dynamic data1DArray)
        {
            if (data1DArray != null && data1DArray.GetType().IsArray)
                return string.Join(", ", data1DArray);
            return "";
        }

        public static void CheckInputFormatFor1DArray(string variableName, string stepName, dynamic data)
        {
            if (data.LastIndexOf(',').Equals(data.Length - 1))
                throw new Exception($"{stepName}.{variableName}: Input not entered in the right format.");
        }

        public static int CheckInputFormatFor2DArray(string variableName, string stepName, dynamic data)
        {
            string[] split2DArrayString = data.Replace(" ", "").Split(new string[] { "(", "),(", ")" }, StringSplitOptions.RemoveEmptyEntries);

            var arrayRows = split2DArrayString.Select(x => x.Split(','));

            int columnLength = GetColumnLength(variableName, stepName, data);

            foreach (var row in arrayRows)
            {
                if (row.Length.Equals(columnLength))
                {
                    continue;
                }
                else
                {
                    throw new Exception($"{stepName}.{variableName}: Input not entered in the right format.");
                }
            }

            return columnLength;
        }

    }
}
