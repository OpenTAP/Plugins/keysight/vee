﻿using System;
using System.Collections.Generic;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for numerical limit check.
    /// </summary>
    public static class LimitCheckHelper
    {
        private static bool compare(dynamic testValue, double limit, LimitOperator limitOperator)
        {
            switch (limitOperator)
            {
                case LimitOperator.Equal:
                    return limit == testValue;
                case LimitOperator.NotEqual:
                    return limit != testValue;
                case LimitOperator.GreaterThan:
                    return limit > testValue;
                case LimitOperator.GreaterThanOrEqual:
                    return limit >= testValue;
                case LimitOperator.LessThan:
                    return limit < testValue;
                case LimitOperator.LessThanOrEqual:
                    return limit <= testValue;
                default:
                    return false;
            }
        }

        public static bool Range(dynamic testValue, double upperLimit, LimitOperator upperOperator, double lowerLimit, LimitOperator lowerOperator)
        {
            return SingleLimit(testValue, upperLimit, upperOperator) && SingleLimit(testValue, lowerLimit, lowerOperator);
        }

        public static bool SingleLimit(dynamic testValue, double limit, LimitOperator limitOperator)
        {
            var valueType = testValue?.GetType();
            string valueTypeName = valueType?.Name;

            if (valueTypeName == null || valueTypeName.Contains("String") || valueTypeName.Contains("Boolean") || valueTypeName.Contains("System.__ComObject"))
            {
                // null or wrong data type
                return false;
            }
            else if (valueType.IsArray)
            {
                // array
                foreach (var value in testValue)
                {
                    if (!compare(value, limit, limitOperator))
                        return false;
                }
                return true;
            }
            else
            {
                // scalar
                return compare(testValue, limit, limitOperator);
            }
        }

        public static bool Tolerance(dynamic testValue, double nominalValue, double plusTolerance, double minusTolerance)
        {
            double upperLimit = nominalValue + plusTolerance;
            double lowerLimit = nominalValue - minusTolerance;

            return Range(testValue, upperLimit, LimitOperator.GreaterThanOrEqual, lowerLimit, LimitOperator.LessThanOrEqual);
        }

        public static bool TolerancePercent(dynamic testValue, double nominalValue, double plusPercent, double minusPercent)
        {
            double upperLimit = nominalValue + (nominalValue * plusPercent / 100);
            double lowerLimit = nominalValue - (nominalValue * minusPercent / 100);

            return Range(testValue, upperLimit, LimitOperator.GreaterThanOrEqual, lowerLimit, LimitOperator.LessThanOrEqual);
        }
    }
}
