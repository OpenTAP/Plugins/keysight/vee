﻿namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Helper class for string limit check.
    /// </summary>
    public static class StringCheckHelper
    {
        private static bool check(string testString, string checkString, StringOperator stringOperator)
        {
            if (string.IsNullOrWhiteSpace(testString) || string.IsNullOrWhiteSpace(checkString))
            {
                return false;
            }

            string trimmedTestString = testString.Trim();
            string trimmedCheckString = checkString.Trim();

            switch (stringOperator)
            {
                case StringOperator.Equals:
                    return trimmedTestString.Equals(trimmedCheckString);
                case StringOperator.Contains:
                    return trimmedTestString.Contains(trimmedCheckString);
                default:
                    return false;
            }
        }

        public static bool StringCheck(dynamic testValue, string checkString, StringOperator stringOperator)
        {
            var valueType = testValue?.GetType();
            string valueTypeName = valueType?.Name;

            if (valueTypeName == null || !valueTypeName.Contains("String"))
            {
                // null or wrong data type
                return false;
            }
            else if (valueType.IsArray)
            {
                // array
                foreach (var value in testValue)
                {
                    if (!check(value, checkString, stringOperator))
                        return false;
                }
                return true;
            }
            else
            {
                // scalar
                return check(testValue, checkString, stringOperator);
            }
        }
    }
}
