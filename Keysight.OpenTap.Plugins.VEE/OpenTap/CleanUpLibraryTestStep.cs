﻿// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System.Linq;
using CallableVEE;

namespace OpenTap.Plugins.VeeIntegration
{
    [Display("Clean Up Library", Group: "VEE", Description: "Delete all low-level libraries.")]
    public class CleanUpLibraryTestStep : TestStep
    {
        public CleanUpLibraryTestStep() { }

        public override void Run()
        {
            if (VeeTypeDataProvider.VeeDescriptorCollection?.Descriptors.Count == 0 || ComponentSettings.GetCurrent(typeof(VeeSettings)) == null) return;

            var veeCallableServer = VeeTypeDataProvider.VeeDescriptorCollection?.Descriptors?.First().Value?.UserFunction?.CallServer;

            if (veeCallableServer == null) return;

            var topLevelLibLoaded = VeeSettings.Current.VeeFiles.Where(x => x.IsEnabled && string.IsNullOrEmpty(x.Error)).Select(y => y.FilePath);

            foreach (Library lib in veeCallableServer.Libraries)
            {
                if (!topLevelLibLoaded.Contains(lib.Path))
                    lib.Unload();
            }
        }
    }
}
