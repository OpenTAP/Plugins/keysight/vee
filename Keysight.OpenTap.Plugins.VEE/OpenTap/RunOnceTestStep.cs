﻿// Copyright:   Copyright 2020 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// A generic test step that only runs once after the test plan is loaded.
    /// </summary>
    [Display("Run Once", Group: "Flow Control", Description: "Runs its child steps sequentially, but only once after test plan is loaded.")]
    [AllowAnyChild]
    public class RunOnceTestStep : TestStep
    {
        bool wasRun = false;

        public RunOnceTestStep() { }

        public override void Run()
        {
            if (!wasRun)
            {
                RunChildSteps();
                wasRun = true;
            }
        }
    }
}
