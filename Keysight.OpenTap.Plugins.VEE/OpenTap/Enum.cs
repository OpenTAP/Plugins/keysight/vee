﻿namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Type of numerical limit check
    /// </summary>
    public enum LimitType
    {
        [Display("Range", "Compares the testing value to a lower and upper limit.")]
        Range,
        [Display("Limit", "Compares the testing value to one limit.")]
        Limit,
        [Display("Tolerance", "Compares the testing value to the nominal value plus and minus a tolerance.")]
        Tolerance,
        [Display("%Tolerance", "Compares the testing value to the nominal value plus and minus a percentage of the nominal value.")]
        TolerancePercentage
    }

    /// <summary>
    /// Type of numerical comparison operator
    /// </summary>
    public enum LimitOperator
    {
        [Display("Limit > Testing Value")]
        GreaterThan,
        [Display("Limit >= Testing Value")]
        GreaterThanOrEqual,
        [Display("Limit < Testing Value")]
        LessThan,
        [Display("Limit <= Testing Value")]
        LessThanOrEqual,
        [Display("Limit == Testing Value")]
        Equal,
        [Display("Limit != Testing Value")]
        NotEqual
    }

    /// <summary>
    /// Type of VEE data supported in OpenTAP
    /// </summary>
    public enum OpenTapDataType
    {
        [VeeTypeDisplay(Int16)]
        Int16,
        [VeeTypeDisplay(Int32)]
        Int32,
        [VeeTypeDisplay(Int64)]
        Int64,
        [VeeTypeDisplay(Real32)]
        Real32,
        [VeeTypeDisplay(Real64)]
        Real64,
        [VeeTypeDisplay(UInt8)]
        UInt8,
        [VeeTypeDisplay(UInt16)]
        UInt16,
        [VeeTypeDisplay(Boolean)]
        Boolean,
        [VeeTypeDisplay(Text)]
        Text,
        [VeeTypeDisplay(PComplex)]
        PComplex,
        [VeeTypeDisplay(Complex)]
        Complex,
        [VeeTypeDisplay(Waveform)]
        Waveform,
        [VeeTypeDisplay(Spectrum)]
        Spectrum,
        [VeeTypeDisplay(Coord)]
        Coord,
        Any,
        NotSupported
    }

    /// <summary>
    /// Shape of VEE data supported in OpenTAP
    /// </summary>
    public enum OpenTapDataShape
    {
        Scalar,
        [Display("Array 1D")]
        Array1D,
        [Display("Array 2D")]
        Array2D,
        Any,
        NotSupported
    }

    /// <summary>
    /// Type of string comparison operator
    /// </summary>
    public enum StringOperator
    {
        [Display("Equals Keyword")]
        Equals,
        [Display("Contains Keyword")]
        Contains
    }

    /// <summary>
    /// Type of output check
    /// </summary>
    public enum OutputCheckType
    {
        [Display("OFF")]
        Off,
        [Display("Limit Check")]
        LimitCheck,
        [Display("String Check")]
        StringCheck,
        [Display("Boolean Check")]
        BooleanCheck
    }

    /// <summary>
    /// Generic name of VEE test step property
    /// </summary>
    public enum Naming
    {
        DataType,
        DataShape,
        TypeShapeRecord,
        ColumnDisplay
    }

}
