﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The property of the VEE test step that records the selected data type and data shape of an input/output parameter of the VEE user function.
    /// </summary>
    class TypeAndShapeRecord : ValuePlaceholderMember
    {
        public TypeAndShapeRecord(VeeArgument arg, ITypeData declaringType) : base(arg, declaringType)
        {
            Name = $"{VeeArgument.Name}{Naming.TypeShapeRecord}";
        }

        public override void SetValue(object owner, object value = null)
        {
            if (owner is VeeTestStep step)
            {
                var enabledSelectors = step.GetEnabledSelectors(VeeArgument.Name);
                if (enabledSelectors == null || enabledSelectors.Count() != 2)
                    return;

                int delimiterIndex = -1;
                if (step.MemberValues.TryGetValue(this, out object typeShapeRecord))
                    delimiterIndex = typeShapeRecord.ToString().IndexOf(':');

                foreach (var selector in enabledSelectors)
                {
                    if (!step.MemberValues.TryGetValue(selector, out object selection))
                        continue;
                    else if (selector is VeeDataTypeSelector)
                        typeShapeRecord = selection.ToString() + (delimiterIndex > -1 ? typeShapeRecord.ToString().Substring(delimiterIndex) : ":");
                    else if (selector is VeeDataShapeSelector)
                        typeShapeRecord = (delimiterIndex > -1 ? typeShapeRecord.ToString().Substring(0, delimiterIndex + 1) : ":") + selection.ToString();

                    delimiterIndex = typeShapeRecord.ToString().IndexOf(':');
                }
                step.MemberValues[this] = typeShapeRecord;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is TypeAndShapeRecord other)
                return Equals(other.DeclaringType, DeclaringType) && other.Name == Name;
            return false;
        }

        public override int GetHashCode() => DeclaringType.GetHashCode() * 567234 + Name.GetHashCode();
    }
}
