﻿using CallableVEE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The collection of the type information retrieved from the VEE user functions in all loaded VEE files.
    /// </summary>
    internal class VeeDescriptorCollection
    {
        private static HashSet<string> loadedFileName;

        private static VeeInteraction interaction;

        public static TraceSource log = Log.CreateSource("VEE");

        public Dictionary<string, VeeDescriptor> Descriptors { get { return descriptors; } }
        private static Dictionary<string, VeeDescriptor> descriptors;
        
        public VeeDescriptorCollection()
        {
            interaction = interaction ?? (new VeeInteraction());

            descriptors = descriptors ?? new Dictionary<string, VeeDescriptor>();
            loadedFileName = loadedFileName ?? new HashSet<string>();
        }

        /// <summary>
        /// Load single VEE file.
        /// </summary>
        /// <param name="file"></param>
        public void LoadVeeFile(VeeFile file)
        {
            if (!loadedFileName.Contains(file.Name))
            {
                interaction.Load(file.FilePath);
                generateVeeDescriptor(file);
                loadedFileName.Add(file.Name);
            }
        }

        /// <summary>
        /// Load VEE files of current setting.
        /// </summary>
        public bool LoadVeeFile()
        {
            if (ComponentSettings.GetCurrent(typeof(VeeSettings)) == null)
                return false;

            foreach (var file in VeeSettings.Current.VeeFiles)
            {
                if (!string.IsNullOrEmpty(file.Error) || !file.IsEnabled)
                    continue;
                else
                {
                    try
                    {
                        LoadVeeFile(file);
                    }
                    catch (Exception ex)
                    {
                        var loadedLibraries = interaction.Server.Libraries;
                        loadedLibraries[loadedLibraries.Count - 1].Unload();
                        log.Error($"VEE server fails to load the VEE file \"{file.Name}\". Please rectify the VEE file and restart OpenTAP.");
                        log.Error(VeeHelper.GetVeeErrorMessage(ex, file.Name));
                    }
                }
            }
            return true;
        }

        private void generateVeeDescriptor(VeeFile file)
        {
            if (interaction?.Library == null)
                return;

            foreach (UserFunction function in interaction.Library.UserFunctions)
            {
                var descriptor = new VeeDescriptor() { FunctionName = function.Name, Description = function.Description, FunctionType = VeeFunctionTypes.UserFunction, FilePath = file.FilePath, FileName = file.Name, UserFunction = function };
                var veeArguments = new List<VeeArgument>();

                int inputCount = function.InputArguments.Count - 1;
                foreach (Argument argument in function.InputArguments)
                {
                    var veeArgument = new VeeArgument() { Index = inputCount, Input = true, Name = argument.Name, DataShape = argument.DataShape, DataType = argument.DataType, TerminalType = argument.TerminalType };
                    veeArguments.Add(veeArgument);
                    inputCount--;
                }

                int outputCount = function.OutputArguments.Count - 1;
                foreach (Argument argument in function.OutputArguments)
                {
                    var veeArgument = new VeeArgument() { Index = outputCount, Input = false, Name = argument.Name, DataShape = argument.DataShape, DataType = argument.DataType, TerminalType = argument.TerminalType };
                    veeArguments.Add(veeArgument);
                    outputCount--;
                }

                descriptor.Arguments = veeArguments.ToArray();
                Descriptors.Add(VeeTypeData.GenerateTypeDataName(descriptor), descriptor);
            }
        }
    }
}
