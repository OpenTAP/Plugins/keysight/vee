﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The property of the VEE test step that corresponds to the input/output parameter of the VEE user function.
    /// </summary>
    class VeeMemberData : VeeAbstractMember
    {
        public VeeMemberData(VeeArgument arg, ITypeData declaringType, KeyValuePair<Type, object[]> dotNetVeeTypeMatchingPair) : base(arg, declaringType)
        {
            Name = $"{VeeArgument.Name}{dotNetVeeTypeMatchingPair.Key.Name}";
            TypeDescriptor = TypeData.FromType(dotNetVeeTypeMatchingPair.Key);

            // create the attribute list of this member data
            var attribs = new List<object>()
            {
                new DisplayAttribute(VeeArgument.Name, null, VeeArgument.Input ? "Input" : "Output", VeeArgument.Input ? VeeArgument.Index : VeeArgument.Index + 100, false, new [] {VeeArgument.Input ? "Input" : "Output", VeeArgument.Name }),
                new EnabledIfAttribute($"{VeeArgument.Name}{Naming.TypeShapeRecord}", dotNetVeeTypeMatchingPair.Value){HideIfDisabled = true},
                new BrowsableAttribute(true)
            };

            if (!VeeArgument.Input)
            {
                attribs.Add(new OutputAttribute());
                Writable = false;
            }

            Attributes = attribs;
        }


        public override void SetValue(object owner, object value)
        {
            base.SetValue(owner, value);

            // force the refresh of column display member in the step member values
            var columnDisplayMember = new VeeColumnDisplayMember(VeeArgument, DeclaringType);
            columnDisplayMember.SetValue(owner);
        }


        public virtual void SetDefaultValue(object owner, object value)
        {
            base.SetValue(owner, value);
        }

        public override bool Equals(object obj)
        {
            if (obj is VeeMemberData other)
                return Equals(other.DeclaringType, DeclaringType) && other.Name == Name;
            return false;
        }

        public override int GetHashCode() => DeclaringType.GetHashCode() * 321421 + Name.GetHashCode();

    }
}

