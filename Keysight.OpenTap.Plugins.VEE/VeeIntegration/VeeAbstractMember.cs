﻿using System.Collections.Generic;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The abstract property of the VEE test step.
    /// </summary>
    abstract class VeeAbstractMember : IMemberData
    {
        public VeeAbstractMember(VeeArgument arg, ITypeData declaringType)
        {
            VeeArgument = arg;
            DeclaringType = declaringType;
            Writable = true;
            Readable = true;
        }

        public VeeArgument VeeArgument { get; protected set; }

        public ITypeData DeclaringType { get; protected set; }

        public ITypeData TypeDescriptor { get; protected set; }

        public IEnumerable<object> Attributes { get; protected set; }

        public string Name { get; protected set; }

        public virtual void SetValue(object owner, object value)
        {
            if (value != null && owner is VeeTestStep step)
                step.MemberValues[this] = value;
        }

        public virtual object GetValue(object owner)
        {
            if (owner is VeeTestStep step && step.MemberValues.TryGetValue(this, out object value))
                return value;
            return null;
        }

        public virtual bool Writable { get; protected set; }

        public virtual bool Readable { get; protected set; }

        public abstract override bool Equals(object obj);

        public abstract override int GetHashCode();
    }
}
