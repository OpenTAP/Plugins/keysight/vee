﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Contains the information of the VEE file. E.g. file name, file path, etc.
    /// </summary>
    [Display("VEE File", "The configuration of a VEE file.")]
    public class VeeFile : ValidatingObject
    {
        private string name;

        /// <summary>
        /// The name of the VEE file.
        /// </summary>
        [Display("Name", "The name of the VEE file (.vee/.vxe).", Order: 1)]
        [Browsable(true)]
        public string Name
        {
            get => name;
            private set
            {
                name = Path.GetFileNameWithoutExtension(value);
            }
        }

        private string filePath;

        /// <summary>
        /// The absolute path of the VEE file.
        /// </summary>
        [Display("Path", "The path of the VEE file.", Order: 2)]
        [FilePath(fileExtension: "VEE Files (*.vee)|*.vee|Secured VEE Files (*.vxe)|*.vxe")]
        public string FilePath
        {
            get => filePath;
            set
            {
                if (value == null)
                {
                    filePath = "";
                    Name = "";
                }
                else
                {
                    filePath = Path.GetFullPath(value);
                    Name = value;
                }
            }
        }

        /// <summary>
        /// The loading status of the VEE file.
        /// </summary>
        [Display("Enabled", "Enable the loading of the VEE file.", Order: 0)]
        public bool IsEnabled { get; set; } = true;

        public VeeFile()
        {
            Rules.Add(() => File.Exists(FilePath), "The file does not exist.", nameof(FilePath));

            Rules.Add(() =>
            {
                string pathExtension = Path.GetExtension(FilePath);
                return (pathExtension == ".vee" || pathExtension == ".vxe") || !File.Exists(FilePath);
            }, "Unsupported file.", nameof(FilePath));
        }
    }

}
