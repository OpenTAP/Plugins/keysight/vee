﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CallableVEE;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The configuration of the OpenTAP VEE plugin. E.g. VEE files to be loaded.
    /// </summary>
    [Display("VEE", "Settings for the VEE plugin.")]
    public class VeeSettings : ComponentSettings<VeeSettings>
    {
        private bool isInit = true;
        private List<VeeFile> veeFiles;
        private List<VeeFile> loadedVeeFiles;
        private readonly string pathErrorMessage = "VEE file error(s) found.";

        /// <summary>
        /// Collection of VEE files to be loaded.
        /// </summary>
        [Display("VEE File(s)", "A list of VEE files.")]
        public List<VeeFile> VeeFiles
        {
            get
            {
                return veeFiles;
            }
            set
            {
                var cleanList = removeEmptyAndDuplicatePath(value);

                if (isInit)
                {
                    loadedVeeFiles = cleanList.Select(x => new VeeFile() { IsEnabled = x.IsEnabled, FilePath = x.FilePath }).ToList();
                    isInit = false;
                }
                veeFiles = cleanList;
            }
        }

        public VeeSettings()
        {
            veeFiles = new List<VeeFile>();
            loadedVeeFiles = new List<VeeFile>();
            Rules.Add(() => hasNoFilePathError(), pathErrorMessage, nameof(VeeFiles));
            Rules.Add(() => hasNoDuplicateFileName() || !hasNoFilePathError(), "VEE file(s) with duplicate name won't be loaded, please disable or change the duplicate file name(s).", nameof(VeeFiles));
            Rules.Add(() => isPathUnchange() || !hasNoDuplicateFileName() || !hasNoFilePathError(), "VEE file changes won't take effect until OpenTAP is restarted.", nameof(VeeFiles));
        }

        private bool hasNoFilePathError()
        {
            return !VeeFiles.Exists(x => !string.IsNullOrEmpty(x.Error));
        }

        private bool isPathUnchange()
        {
            var enabledCurrentPath = VeeFiles.Where(x => x.IsEnabled && string.IsNullOrEmpty(x.Error));
            var enabledLoadedPath = loadedVeeFiles.Where(x => x.IsEnabled && string.IsNullOrEmpty(x.Error));
            bool isCurrentSameAsLoaded = !enabledCurrentPath.Except(enabledLoadedPath, new PathStringEqualityComparer()).Any();
            bool isLoadedSameAsCurrent = !enabledLoadedPath.Except(enabledCurrentPath, new PathStringEqualityComparer()).Any();
            return isCurrentSameAsLoaded && isLoadedSameAsCurrent;
        }

        private bool hasNoDuplicateFileName()
        {
            return !VeeFiles.Where(p => p.IsEnabled).GroupBy(x => Path.GetFileNameWithoutExtension(x.FilePath)).Any(g => g.Count() > 1);
        }

        private List<VeeFile> removeEmptyAndDuplicatePath(List<VeeFile> veeFiles)
        {
            return veeFiles != null ? veeFiles.Where(x => !string.IsNullOrEmpty(x.FilePath)).Distinct(new PathStringEqualityComparer()).ToList() : new List<VeeFile>();
        }
    }

    class PathStringEqualityComparer : IEqualityComparer<VeeFile>
    {
        public bool Equals(VeeFile x, VeeFile y)
        {
            return x.FilePath == y.FilePath;
        }

        public int GetHashCode(VeeFile obj)
        {
            return obj.FilePath.GetHashCode();
        }
    }

}
