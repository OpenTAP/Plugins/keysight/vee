﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CallableVEE;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Contains the type information retrieved from the VEE user function.
    /// </summary>
    public class VeeDescriptor
    {
        public string FunctionName { get; set; }
        public string Description { get; set; }
        public VeeFunctionTypes FunctionType { get; set; }
        public VeeArgument[] Arguments { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public UserFunction UserFunction { get; set; }
    }
}
