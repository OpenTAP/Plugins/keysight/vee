﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CallableVEE;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Contains the type information retrieved from the input/output parameter of the VEE user function. 
    /// </summary>
    public class VeeArgument
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Index { get; set; }
        public bool Input { get; set; }
        public VeeDataShape DataShape { get; set; }
        public VeeDataType DataType { get; set; }
        public VeeTerminalType TerminalType { get; set; }
        public TypeData OpenTapTypeData { get; set; }
    }
}
