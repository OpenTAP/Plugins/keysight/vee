﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The abstract property of the VEE test step for the value place holder.
    /// </summary>
    abstract class ValuePlaceholderMember : VeeAbstractMember
    {
        public ValuePlaceholderMember(VeeArgument arg, ITypeData declaringType) : base(arg, declaringType)
        {
            TypeDescriptor = TypeData.FromType(typeof(string));
            Attributes = new List<object>()
            {
                new BrowsableAttribute(false),
                new XmlIgnoreAttribute()
            };
            Writable = false;
        }
    }
}
