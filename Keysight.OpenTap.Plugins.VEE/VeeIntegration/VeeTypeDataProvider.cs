﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The type data provider that loads the VEE files and generates the collection of VEE type data.
    /// </summary>
    public class VeeTypeDataProvider : ITypeDataProvider, ITypeDataSearcher
    {
        internal const string PREFIX = "VEE:";

        internal static VeeDescriptorCollection VeeDescriptorCollection;

        public IEnumerable<ITypeData> Types { get; private set; }

        public VeeTypeDataProvider()
        {
            try
            {
                VeeDescriptorCollection = VeeDescriptorCollection ?? (new VeeDescriptorCollection());
            }
            catch (Exception e)
            {
                if (e.Message.Contains("REGDB_E_CLASSNOTREG"))
                    VeeDescriptorCollection.log.Error("Keysight VEE Pro/RunTime 9.33 is not found on this machine. Please install it to use the OpenTAP VEE plugin.");
                else
                    VeeDescriptorCollection.log.Error(e.Message);
            }
        }

        /// <summary>
        /// Locate a type based on string identifier.
        /// </summary>
        /// <param name="identifier">Prefix with "VEE:" if it is a VeeTypeData.</param>
        /// <returns></returns>
        public ITypeData GetTypeData(string identifier)
        {
            if (VeeDescriptorCollection != null && VeeDescriptorCollection.Descriptors.TryGetValue(identifier, out VeeDescriptor descriptor))
            {
                return new VeeTypeData(descriptor);
            }
            return null;
        }

        /// <summary>
        /// Identify if the type data of an object is a VeeTypeData. This is done by checking if its a VeeTestStep with an associated type.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ITypeData GetTypeData(object obj)
        {
            if (obj is VeeTestStep step)
                return step.Type;
            return null;
        }

        /// <summary>
        /// Priority slightly higher than default (required).
        /// </summary>
        public double Priority => 1;

        /// <summary>
        /// Load VEE files and generate VEE type data.
        /// </summary>
        public void Search()
        {
            if (VeeDescriptorCollection == null || !VeeDescriptorCollection.LoadVeeFile())
                return;

            Types = VeeDescriptorCollection.Descriptors.Select(x => new VeeTypeData(x.Value));
        }
    }
}
