﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using CallableVEE;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.IO;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Class that creates, maintains and closes the connection with callable VEE server.
    /// </summary>
    class VeeInteraction : IDisposable
    {
        internal CallServerClass Server { get; private set; }
        Library library;
        Dictionary<string, int> functions;

        public VeeInteraction()
        {
            Server = new CallServerClass();

            var currentProcId = Process.GetCurrentProcess().Id;
            string backupFilePath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\VEE\ProcessDeadLockBackup.txt";
            Directory.CreateDirectory(Directory.GetParent(backupFilePath).ToString());
#if (DEBUG)
            string fileName = @"..\Debug\ProcessDeadLock.txt";
#else
            string fileName = @".\Packages\VEE\ProcessDeadLock.txt";
#endif

            killUnwantedProcess(fileName, backupFilePath);
            updateProcessFile(fileName, currentProcId, backupFilePath);
        }

        private void updateProcessFile(string fileName, int currentProcId, string backupFilePath)
        {
            Process[] localByName = Process.GetProcessesByName("vee").Where(x => x.MainWindowTitle.Equals("")).Concat(Process.GetProcessesByName("veerun").Where(x => x.MainWindowTitle.Equals(""))).ToArray();
            List<string> newProc = new List<string>();
            List<string> Proc = File.ReadAllLines(fileName).ToList();

            newProc = makeProcessList(Proc, localByName, currentProcId);

            File.WriteAllLines(fileName, newProc.ToArray());

            File.WriteAllLines(backupFilePath, newProc.ToArray());
        }

        private List<string> makeProcessList(List<string> proc, Process[] veeProcesses, int currentProcId)
        {
            List<string> newProc = proc;
            foreach (var veeProcess in veeProcesses)
            {
                if (!proc.Exists(x => x.Contains(veeProcess.Id.ToString())))
                    newProc.Add(currentProcId.ToString() + " " + veeProcess.Id.ToString());
            }
            return newProc;
        }

        private void killUnwantedProcess(string fileName, string backUpFilePath)
        {

            if (File.Exists(fileName) || File.Exists(backUpFilePath))
            {
                List<string> newList = new List<string>();
                string[] existingList = new string[] { };

                try
                {
                    existingList = File.ReadAllLines(fileName);
                    if (existingList.Length == 0) throw new Exception();// dummy exception to catch and read the backup file
                }
                catch (Exception)
                {
                    if (File.Exists(backUpFilePath))
                        existingList = File.ReadAllLines(backUpFilePath);
                    else
                        File.Create(backUpFilePath).Close();
                    File.Create(fileName).Close();
                }

                foreach (var item in existingList)
                {
                    if (!deleteDeadLockProcess(item))
                    {
                        newList.Add(item);
                    }
                }
                File.WriteAllLines(fileName, newList.ToArray());//write updated list to the deadlock process

            }
            else
            {
                File.Create(fileName).Close();
                File.Create(backUpFilePath).Close();
            }
        }

        private bool deleteDeadLockProcess(string item)
        {
            bool deleted = false;
            var temp = item.Split(' ');
            try
            {
                var parentProc = Process.GetProcessById(Convert.ToInt32(temp[0]));
            }
            catch (ArgumentException)// if process id is not present, it throws this exception
            {
                deleted = true;
                try
                {
                    var veeProc = Process.GetProcessById(Convert.ToInt32(temp[1]));
                    veeProc.Kill();//Kill the unwanted VEE instance
                    Thread.Sleep(100);

                }
                catch (ArgumentException)// if process id is not present, it throws this exception
                {
                    //do nothing as the exception is caught since the VEE process is not available
                }
            }
            return deleted;
        }

        public void Load(string path)
        {
            library = Server.Libraries.Load(path);
        }

        public object[] Call(string functionName, params object[] inputs)
        {
            string lowerName = functionName.Trim().ToLower();
            if (!this.functions.ContainsKey(lowerName))
            {
                throw new Exception("Function Not Found"); // does this have a VEE error number?
            }
            var function = library.UserFunctions[this.functions[lowerName]];


            object outputs = new object[function.OutputArguments.Count];
            function.Call(inputs, ref outputs);

            return (object[])outputs;
        }

        public int GetVeeErrorCode(COMException ex)
        {
            return GetVeeErrorCode(ex.ErrorCode);
        }

        public int GetVeeErrorCode(int hresult)
        {
            // from TypeLib: VeeConstants.veeErrorOffset = 0x80040200
            const uint VEE_ERROR_OFFSET = 0x80040200;
            return (int)(hresult - VEE_ERROR_OFFSET);
        }

        private Dictionary<string, int> findFunctions()
        {
            var functions = new Dictionary<string, int>();
            for (int i = 0; i < this.library.UserFunctions.Count; i++)
            {
                functions[this.library.UserFunctions[i].Name.ToLower().Trim()] = i;
            }
            return functions;
        }
        public Library Library
        {
            get
            {
                return library;
            }
        }

        public void PrintFunctions()
        {
            foreach (UserFunction function in library.UserFunctions)
            {
                Console.WriteLine("----");
                Console.WriteLine("Name: {0}", function.Name);
                Console.WriteLine("Description: {0}", function.Description);

                Console.WriteLine("-> Inputs");
                int inputI = 0;
                foreach (Argument argument in function.InputArguments)
                {
                    Console.WriteLine("\tName: {0}, Index: {1}, TerminalType: {2}, DataType: {3}, DataShape: {4}",
                        argument.Name,
                        inputI,
                        argument.TerminalType,
                        argument.DataType,
                        argument.DataShape
                        );
                    inputI++;
                }

                Console.WriteLine("<- Outputs");
                int outputI = 0;
                foreach (Argument argument in function.OutputArguments)
                {
                    Console.WriteLine("\tName: {0}, Index: {1}, TerminalType: {2}, DataType: {3}, DataShape: {4}",
                        argument.Name,
                        outputI,
                        argument.TerminalType,
                        argument.DataType,
                        argument.DataShape
                        );
                    outputI++;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                if (this.library != null)
                {
                    try
                    {
                        this.library.Unload();
                        // TODO: To release all loaded libraries?
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Some problem unloading VEE library: {0}", ex.ToString());
                    }
                }
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~VeeInteraction()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
