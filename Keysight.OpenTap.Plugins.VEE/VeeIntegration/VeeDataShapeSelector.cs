﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The data shape selector property of the VEE test step.
    /// </summary>
    class VeeDataShapeSelector : VeeSelectorMember
    {
        public VeeDataShapeSelector(VeeArgument arg, ITypeData declaringType, KeyValuePair<string, Tuple<object[], object[]>> caseSupportedTypes, bool isWritable = true) : base(arg, declaringType)
        {
            Name = $"{VeeArgument.Name}{Naming.DataShape}{caseSupportedTypes.Key}";
            TypeDescriptor = TypeData.FromType(typeof(OpenTapDataShape));
            Attributes = new List<object>
            {
                new DisplayAttribute($"Data Shape", null, VeeArgument.Input ? "Input" : "Output" , VeeArgument.Input ? VeeArgument.Index + 0.2 : VeeArgument.Index + 100.2, false, new [] {VeeArgument.Input ? "Input" : "Output", VeeArgument.Name }),
                new AvailableValuesAttribute($"AvailableDataShapes{caseSupportedTypes.Key}"),
                new EnabledIfAttribute($"{VeeArgument.Name}{Naming.DataType}", caseSupportedTypes.Value.Item1 ) {HideIfDisabled = true},
                new BrowsableAttribute(true)
            };
            Writable = isWritable;
        }

        public override bool Equals(object obj)
        {
            if (obj is VeeDataShapeSelector other)
                return Equals(other.DeclaringType, DeclaringType) && other.Name == Name;
            return false;
        }

        public override int GetHashCode() => DeclaringType.GetHashCode() * 628531 + Name.GetHashCode();

    }
}
