﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The property of the VEE test step that displays the value of the selected property in the test plan panel.
    /// </summary>
    class VeeColumnDisplayMember : ValuePlaceholderMember
    {
        public VeeColumnDisplayMember(VeeArgument arg, ITypeData declaringType) : base(arg, declaringType)
        {
            Name = $"{VeeArgument.Name}{Naming.ColumnDisplay}";
            Attributes = new List<object>(Attributes)
            {
                // Display attribute's name and groups must be the same with the display attribute of the corresponding VeeMemberData
                new DisplayAttribute(VeeArgument.Name, Groups: new string[]{ VeeArgument.Input ? "Input" : "Output", VeeArgument.Name })
            };
        }

        public override void SetValue(object owner, object value = null)
        {
            if (owner is VeeTestStep step)
            {
                var linkedMember = step.GetEnabledVeeMemberData(VeeArgument.Name);
                if (linkedMember != null && step.MemberValues.TryGetValue(linkedMember, out object linkedValue))
                {
                    if (linkedValue.GetType().IsArray)
                        step.MemberValues[this] = InputOutputArgumentHelper.GetDisplay1DArray(linkedValue);
                    else
                        step.MemberValues[this] = linkedValue.ToString();
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is VeeColumnDisplayMember other)
                return Equals(other.DeclaringType, DeclaringType) && other.Name == Name;
            return false;
        }

        public override int GetHashCode() => DeclaringType.GetHashCode() * 984353 + Name.GetHashCode();
    }
}
