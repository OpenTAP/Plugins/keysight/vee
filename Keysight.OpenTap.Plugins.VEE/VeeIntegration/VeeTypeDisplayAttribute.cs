﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// Display attribute specific to the VEE data type.
    /// </summary>
    internal class VeeTypeDisplayAttribute : DisplayAttribute
    {
        public VeeTypeDisplayAttribute(OpenTapDataType Type, string Group = null, double Order = -10000, bool Collapsed = false, string[] Groups = null) : base(Name: Type.ToString(), Description: getTypeDescription(Type), Group: Group, Order: Order, Collapsed: Collapsed, Groups: Groups) { }

        private static string getTypeDescription(OpenTapDataType type)
        {
            switch (type)
            {
                case OpenTapDataType.Int16:
                case OpenTapDataType.Int32:
                case OpenTapDataType.Int64:
                case OpenTapDataType.Real32:
                case OpenTapDataType.Real64:
                case OpenTapDataType.UInt8:
                case OpenTapDataType.UInt16:
                case OpenTapDataType.Boolean:
                case OpenTapDataType.Text:
                    return "Data Format:-\n" +
                        "Scalar: x\n" +
                        "Array 1D: x, y, z,...\n" +
                        "Array 2D: (x0, y0, z0,...), (x1, y1, z1,...),...";
                case OpenTapDataType.PComplex:
                    return "Data Format:-\n" +
                        "Scalar: magnitude, phase\n" +
                        "Array 1D: (mag0, phase0), (mag1, phase1),...";
                case OpenTapDataType.Complex:
                    return "Data Format:-\n" +
                        "Scalar: real, imaginary\n" +
                        "Array 1D: (real0, imag0), (real1, imag1),...";
                case OpenTapDataType.Waveform:
                    return "Data Format:-\n" +
                        "Array 1D: x, y, z,...";
                case OpenTapDataType.Spectrum:
                    return "Data Format:-\n" +
                        "Array 1D: (mag0, phase0), (mag1, phase1),...";
                case OpenTapDataType.Coord:
                    return "Data Format:-\n" +
                        "Scalar: x, y, z,...\n" +
                        "Array 1D: (x0, y0, z0,...), (x1, y1, z1,...),...";
                default:
                    return null;
            }
        }
    }
}
