﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The abstract property of the VEE test step for the data selector.
    /// </summary>
    abstract class VeeSelectorMember : VeeAbstractMember
    {
        public VeeSelectorMember(VeeArgument arg, ITypeData declaringType) : base(arg, declaringType) { }

        public override void SetValue(object owner, object value)
        {
            base.SetValue(owner, value);

            // update the record of data type and data shape of the selected argument
            var typeShapeRecord = new TypeAndShapeRecord(VeeArgument, DeclaringType);
            typeShapeRecord.SetValue(owner);

            // force the refresh of column display member in the step member values
            var columnDisplayMember = new VeeColumnDisplayMember(VeeArgument, DeclaringType);
            columnDisplayMember.SetValue(owner);
        }
    }
}
