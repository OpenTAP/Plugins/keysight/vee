﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// The data type selector property of the VEE test step.
    /// </summary>
    class VeeDataTypeSelector : VeeSelectorMember
    {
        public VeeDataTypeSelector(VeeArgument arg, ITypeData declaringType, string shape, bool isWritable = true) : base(arg, declaringType)
        {
            Name = $"{VeeArgument.Name}{Naming.DataType}";
            TypeDescriptor = TypeData.FromType(typeof(OpenTapDataType));
            Attributes = new List<object>
            {
                new DisplayAttribute($"Data Type", null, VeeArgument.Input ? "Input" : "Output", VeeArgument.Input ? VeeArgument.Index + 0.1 : VeeArgument.Index + 100.1, false, new [] {VeeArgument.Input ? "Input" : "Output", VeeArgument.Name }),
                //default shape is Array1D as it is supported for all supprorted dataTypes
                new AvailableValuesAttribute($"AvailableDataTypesFor{shape}"),
                new BrowsableAttribute(true)
            };
            Writable = isWritable;
        }

        public override bool Equals(object obj)
        {
            if (obj is VeeDataTypeSelector other)
                return Equals(other.DeclaringType, DeclaringType) && other.Name == Name;
            return false;
        }

        public override int GetHashCode() => DeclaringType.GetHashCode() * 756745 + Name.GetHashCode();
    }
}
