﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenTap;
using CallableVEE;
using System.ComponentModel;

namespace OpenTap.Plugins.VeeIntegration
{
    /// <summary>
    /// A placeholder test step that holds the instance of the VEE type data.
    /// </summary>
    class VeeTestStep : TestStep
    {
        public TraceSource VeeLog;

        #region CONST
        private const string PLUS = "Plus (+)";
        private const string MINUS = "Minus (-)";
        private const string LIMIT_CHECK_GROUP = "Output Check";
        private const double LIMIT_ORDER = 201;
        private const double OUTPUT_CHECK_ORDER = 200;
        private const string VALUE_DESCRIPTION = "The value to be used in the checking process.";
        private const string OPERATOR_DESCRIPTION = "The operator to be used in the checking process.";
        #endregion

        #region Output_Check

        #region Range

        public IEnumerable<LimitOperator> GetRangeOperators(bool hasGreater = true)
        {
            return EnumHelper.GetValues<LimitOperator>().Where(x => x.ToString().ToLower().Contains(hasGreater ? "greater" : "less"));
        }

        public IEnumerable<LimitOperator> UpperRangeOperators => GetRangeOperators(true);

        [Display("Upper Limit Operator", OPERATOR_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.1)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Range, HideIfDisabled = true)]
        [AvailableValues(nameof(UpperRangeOperators))]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public LimitOperator UpperRangeOperator { get; set; } = LimitOperator.GreaterThanOrEqual;

        [Display("Upper Limit", VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.2)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Range, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double UpperRangeLimit { get; set; } = 1;
        public IEnumerable<LimitOperator> LowerRangeOperators => GetRangeOperators(false);

        [Display("Lower Limit Operator", OPERATOR_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.3)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Range, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        [AvailableValues(nameof(LowerRangeOperators))]
        public LimitOperator LowerRangeOperator { get; set; } = LimitOperator.LessThanOrEqual;

        [Display("Lower Limit", VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.4)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Range, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double LowerRangeLimit { get; set; } = 0;

        #endregion

        #region Single_Limit

        [Display("Single Limit Operator", OPERATOR_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.1)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Limit, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public LimitOperator SingleOperator { get; set; } = LimitOperator.GreaterThanOrEqual;

        [Display("Single Limit", VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.2)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Limit, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double SingleLimit { get; set; } = 1;

        #endregion

        #region Tolerance

        [Display("Nominal Value", VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.1)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Tolerance, LimitType.TolerancePercentage, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double NominalValue { get; set; } = 0.5;

        [Display(PLUS, VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.2)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Tolerance, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double PlusTolerance { get; set; } = 1;

        [Display(MINUS, VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.3)]
        [EnabledIf(nameof(SelectedLimitType), LimitType.Tolerance, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double MinusTolerance { get; set; } = 1;

        [Display(PLUS, VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.4)]
        [Unit("%")]
        [EnabledIf(nameof(SelectedLimitType), LimitType.TolerancePercentage, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double PlusTolerancePercent { get; set; } = 1;

        [Display(MINUS, VALUE_DESCRIPTION, Groups: new string[] { LIMIT_CHECK_GROUP }, Order: LIMIT_ORDER + 0.5)]
        [Unit("%")]
        [EnabledIf(nameof(SelectedLimitType), LimitType.TolerancePercentage, HideIfDisabled = true)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public double MinusTolerancePercent { get; set; } = 1;

        #endregion

        public bool IsOutputPresent => OutputNames.ToList().Count > 0;
        [Display("Type of Output Check", "The type of the output check.", LIMIT_CHECK_GROUP, Order: OUTPUT_CHECK_ORDER + 0.1)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        public OutputCheckType OutputCheckType { get; set; } = OutputCheckType.Off;

        public IEnumerable<string> OutputNames { get; }

        [Display("Output", "Select the output to be checked.", LIMIT_CHECK_GROUP, Order: OUTPUT_CHECK_ORDER + 0.2)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.StringCheck, OutputCheckType.BooleanCheck, OutputCheckType.LimitCheck, HideIfDisabled = true)]
        [Browsable(true)]
        public string OutputToBeChecked { get; private set; }

        [Display("Type of Limit Check", "Select the type of the limit checking.", LIMIT_CHECK_GROUP, Order: LIMIT_ORDER)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.LimitCheck, HideIfDisabled = true)]
        public LimitType SelectedLimitType { get; set; } = LimitType.Range;

        [Display("String Operator", OPERATOR_DESCRIPTION, LIMIT_CHECK_GROUP, Order: LIMIT_ORDER + 0.1)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.StringCheck, HideIfDisabled = true)]
        public StringOperator CheckStringOperator { get; set; } = StringOperator.Equals;

        [Display("Keyword", VALUE_DESCRIPTION, LIMIT_CHECK_GROUP, Order: LIMIT_ORDER + 0.2)]
        [EnabledIf((nameof(IsOutputPresent)), HideIfDisabled = true)]
        [EnabledIf(nameof(OutputCheckType), OutputCheckType.StringCheck, HideIfDisabled = true)]
        public string Keyword { get; set; } = "text";

        #endregion

        public IEnumerable<OpenTapDataType> AvailableDataTypesForScalar { get; } = EnumHelper.GetValues<OpenTapDataType>().Where(x => x != OpenTapDataType.Any && x != OpenTapDataType.Waveform && x != OpenTapDataType.Spectrum && x != OpenTapDataType.NotSupported);
        public IEnumerable<OpenTapDataType> AvailableDataTypesForArray1D { get; } = EnumHelper.GetValues<OpenTapDataType>().Where(x => x != OpenTapDataType.Any && x != OpenTapDataType.NotSupported);
        public IEnumerable<OpenTapDataType> AvailableDataTypesForArray2D { get; } = EnumHelper.GetValues<OpenTapDataType>().Where(x => x != OpenTapDataType.Any && x != OpenTapDataType.Waveform && x != OpenTapDataType.Spectrum && x != OpenTapDataType.Complex && x != OpenTapDataType.PComplex && x != OpenTapDataType.Coord && x != OpenTapDataType.NotSupported);
        public IEnumerable<OpenTapDataShape> AvailableDataShapesCase1 { get; } = EnumHelper.GetValues<OpenTapDataShape>().Where(x => x != OpenTapDataShape.Any && x != OpenTapDataShape.NotSupported);
        public IEnumerable<OpenTapDataShape> AvailableDataShapesCase2 { get; } = EnumHelper.GetValues<OpenTapDataShape>().Where(x => x == OpenTapDataShape.Array1D && x != OpenTapDataShape.NotSupported);
        public IEnumerable<OpenTapDataShape> AvailableDataShapesCase3 { get; } = EnumHelper.GetValues<OpenTapDataShape>().Where(x => x != OpenTapDataShape.Array2D && x != OpenTapDataShape.Any && x != OpenTapDataShape.NotSupported);

        internal VeeTypeData Type { get; }

        /// <summary>
        /// This stores all the configured value for the test step dynamic properties.
        /// </summary>
        internal Dictionary<IMemberData, object> MemberValues = new Dictionary<IMemberData, object>();

        public bool IsRunTime { get; }

        [Display("Debug", "Enable debugging of VEE test step.", "Common")]
        [EnabledIf(nameof(IsRunTime), false, HideIfDisabled = true)]
        public bool Debug { get; set; } = false;

        [Display("Publish Output to Result Listener", "Enable/disable publishing Output to Result Listener.", "Common")]
        public bool OutputToResult { get; set; } = true;

        public VeeTestStep(VeeTypeData typedata)
        {
            Type = typedata;
            Name = typedata.VeeDescriptor.FunctionName;

            VeeLog = OpenTap.Log.CreateSource($"VEE.{typedata.VeeDescriptor.FileName}.{Name}");
            IsRunTime = Type.VeeDescriptor.UserFunction.CallServer.Path.Contains("veerun.exe");

            OutputNames = new List<string>(Type.VeeDescriptor.Arguments.Where(x => !x.Input).OrderBy(y => y.Index).Select(z => z.Name));
            if (OutputNames.ToList().Count != 0)
                OutputToBeChecked = OutputNames.First();
            else
                OutputToBeChecked = "";

            setDefaultValue();
            var typeMembers = Type.GetMembers();

            var boolListMembers = typeMembers.Where(x => x.TypeDescriptor == TypeData.FromType(typeof(List<bool>)));
            foreach (var boolList in boolListMembers)
            {
                // instantiate bool list
                boolList.SetValue(this, new List<bool>());
            }

            var stringListMembers = typeMembers.Where(x => x.TypeDescriptor == TypeData.FromType(typeof(List<string>)));
            foreach (var stringList in stringListMembers)
            {
                // instantiate string list
                stringList.SetValue(this, new List<string>());
            }

            var selectorMembers = typeMembers.Where(x => x is VeeDataTypeSelector || x is VeeDataShapeSelector).Select(y => y as VeeAbstractMember);

            foreach (var arg in Type.VeeDescriptor.Arguments)
            {
                var typeSelector = selectorMembers.Where(x => x is VeeDataTypeSelector && x.VeeArgument == arg).FirstOrDefault();
                var shapeSelectorList = selectorMembers.Where(x => x is VeeDataShapeSelector && x.VeeArgument == arg);

                var dataType = DataTypeHelper.GetOpenTapDataType(arg.DataType);
                var dataShape = DataTypeHelper.GetOpenTapDataShape(arg.DataShape);

                if (dataType == OpenTapDataType.Any && dataShape == OpenTapDataShape.Any)
                {
                    // default value
                    typeSelector.SetValue(this, arg.Input ? OpenTapDataType.Text : OpenTapDataType.Real64);
                    setDataShapeValuesForAny(shapeSelectorList);
                }
                else if (dataType == OpenTapDataType.Any)
                {
                    // selected shape with any type
                    typeSelector.SetValue(this, arg.Input ? OpenTapDataType.Text : OpenTapDataType.Real64);
                    foreach (var shapeSelector in shapeSelectorList)
                    {
                        shapeSelector.SetValue(this, dataShape);
                    }
                }
                else if (dataShape == OpenTapDataShape.Any)
                {
                    // selected type with any shape
                    typeSelector.SetValue(this, dataType);
                    setDataShapeValuesForAny(shapeSelectorList);
                }
                else
                {
                    typeSelector.SetValue(this, dataType);

                    string shapeCase = DataTypeHelper.GetShapeCase(dataType);

                    if (!(shapeCase.Equals("Case1") && AvailableDataShapesCase1.ToList().Contains(dataShape)) && !(shapeCase.Equals("Case2") && AvailableDataShapesCase2.ToList().Contains(dataShape)) && !(shapeCase.Equals("Case3") && AvailableDataShapesCase3.ToList().Contains(dataShape)))
                    {
                        throw new Exception($"{Name}.{arg.Name}: Input datashape \"{dataShape}\" is not supported in OpenTap.");
                    }

                    foreach (var shapeSelector in shapeSelectorList)
                    {
                        shapeSelector.SetValue(this, dataShape);
                    }
                }

            }

            Rules.Add(() => !(Keyword == null), "Please insert the keyword.", nameof(Keyword));
            Rules.Add(() => LowerRangeLimit < UpperRangeLimit, "Lower limit must be lower than upper limit.", nameof(LowerRangeLimit));

        }

        public override void Run()
        {
            if (!string.IsNullOrEmpty(this.Error))
            {
                VeeLog.Error($"{Name}: Error(s) found.");
                this.UpgradeVerdict(Verdict.Aborted);
                return;
            }

            var desc = Type.VeeDescriptor;

            if (!IsRunTime)
                desc.UserFunction.CallServer.Debug = Debug;

            VeeLog.Info($"{Name}: Calling VEE UserFunction.");
            object outputs = new object[desc.Arguments.Where(x => !x.Input).Count()];
            try
            {
                desc.UserFunction.Call(createInputArguments(desc).ToArray(), ref outputs);

                getOutputArgumentsAndPublish(desc, outputs);
            }
            catch (Exception ex)
            {
                VeeLog.Error(VeeHelper.GetVeeErrorMessage(ex, desc.FileName));
                UpgradeVerdict(Verdict.Aborted);
                setDefaultValue(true);
            }
            VeeLog.Flush();
            // wait for the process to exit.
        }

        #region Default_Value

        private void setDefaultValue(bool outputOnly = false)
        {
            IEnumerable<IMemberData> members;

            if (outputOnly)
                members = Type.GetMembers().Where(x => x is VeeMemberData && !((VeeAbstractMember)x).VeeArgument.Input);
            else
                members = Type.GetMembers().Where(x => x is VeeMemberData);

            foreach (var mem in members)
            {
                ((VeeMemberData)mem).SetDefaultValue(this, defaultValues[(TypeData)mem.TypeDescriptor]);
            }
        }

        internal IMemberData GetEnabledVeeMemberData(string argumentName)
        {
            return Type.GetMembers().Where(x => x is VeeMemberData && x.Name.Contains(argumentName) && EnabledIfAttribute.IsEnabled(x, this)).FirstOrDefault();
        }

        internal IEnumerable<IMemberData> GetEnabledSelectors(string argumentName)
        {
            return Type.GetMembers().Where(x => x is VeeSelectorMember && x.Name.Contains(argumentName) && EnabledIfAttribute.IsEnabled(x, this) && MemberValues.ContainsKey(x));
        }

        private Dictionary<TypeData, object> defaultValues = new Dictionary<TypeData, object>
        {
            {TypeData.FromType(typeof(string)), ""},
            {TypeData.FromType(typeof(short)), (short)0},
            {TypeData.FromType(typeof(short[])), new short[]{0} },
            {TypeData.FromType(typeof(int)), 0},
            {TypeData.FromType(typeof(int[])), new int[]{0} },
            {TypeData.FromType(typeof(long)), (long)0},
            {TypeData.FromType(typeof(long[])), new long[]{0} },
            {TypeData.FromType(typeof(float)), (float)0},
            {TypeData.FromType(typeof(float[])), new float[]{0} },
            {TypeData.FromType(typeof(double)), (double)0},
            {TypeData.FromType(typeof(double[])), new double[]{0} },
            {TypeData.FromType(typeof(byte)), (byte)0},
            {TypeData.FromType(typeof(byte[])), new byte[]{0} },
            {TypeData.FromType(typeof(ushort)), (ushort)0},
            {TypeData.FromType(typeof(ushort[])), new ushort[]{0} },
            {TypeData.FromType(typeof(bool)), false}
        };

        #endregion

        #region Output_Check_Functions

        private bool outputCheck(object outputValue)
        {
            string valueTypeName = outputValue?.GetType()?.Name;
            string LiteralString = "String";
            string LiteralBool = "Boolean";

            if (valueTypeName == null)
                throw new OutputCheckException("The output is empty.");
            else if (OutputCheckType == OutputCheckType.LimitCheck && !valueTypeName.Contains(LiteralString) && !valueTypeName.Contains(LiteralBool))
                return limitCheck(outputValue);
            else if (OutputCheckType == OutputCheckType.StringCheck && valueTypeName.Contains(LiteralString))
                return StringCheckHelper.StringCheck(outputValue, Keyword, CheckStringOperator);
            else if (OutputCheckType == OutputCheckType.BooleanCheck && valueTypeName.Contains(LiteralBool))
                return BoolCheckHelper.BoolCheck(outputValue);
            else
                throw new OutputCheckException($"The selected output check is not applicable to the output type \"{valueTypeName}\".");
        }

        private bool limitCheck(dynamic testValue)
        {
            switch (SelectedLimitType)
            {
                case LimitType.Limit:
                    return LimitCheckHelper.SingleLimit(testValue, SingleLimit, SingleOperator);
                case LimitType.Range:
                    return LimitCheckHelper.Range(testValue, UpperRangeLimit, UpperRangeOperator, LowerRangeLimit, LowerRangeOperator);
                case LimitType.Tolerance:
                    return LimitCheckHelper.Tolerance(testValue, NominalValue, PlusTolerance, MinusTolerance);
                case LimitType.TolerancePercentage:
                    return LimitCheckHelper.TolerancePercent(testValue, NominalValue, PlusTolerancePercent, MinusTolerancePercent);
                default:
                    return false;
            }
        }

        #endregion

        #region Input_Output_Arguments

        private List<object> createInputArguments(VeeDescriptor desc)
        {
            List<object> inputList = new List<object>();

            foreach (var inp in desc.Arguments.Where(x => x.Input).OrderBy(y => y.Index))
            {
                var veeDataType = getDataType(inp.Name, inp.DataType);
                var veeDataShape = getDataShape(inp.Name, inp.DataShape, DataTypeHelper.GetShapeCase(veeDataType));

                dynamic data = GetEnabledVeeMemberData(inp.Name)?.GetValue(this);

                if (data == null)
                {
                    throw new Exception($"{Name}.{inp.Name}: Input not entered.");
                }

                VeeDataContainer specialInp = desc.UserFunction.CallServer.CreateVeeDataContainer();

                if (veeDataShape.Equals(OpenTapDataShape.Scalar))
                {
                    if (veeDataType.Equals(OpenTapDataType.PComplex))
                    {
                        specialInp.CreatePolarComplexScalar(data[0], data[1]);
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Complex))
                    {
                        specialInp.CreateComplexScalar(data[0], data[1]);
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Coord))
                    {
                        specialInp.CreateCoordScalar(data);
                        inputList.Add(specialInp);
                    }
                    else
                    {
                        inputList.Add(data);
                    }
                }
                else if (veeDataShape.Equals(OpenTapDataShape.Array1D))
                {
                    if (veeDataType.Equals(OpenTapDataType.PComplex))
                    {
                        specialInp.CreatePolarComplex1DArray(InputOutputArgumentHelper.Get2DArray(Name, inp.Name, data, DataTypeHelper.GetDataTypefor2DArrayElements(veeDataType)));
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Complex))
                    {
                        specialInp.CreateComplex1DArray(InputOutputArgumentHelper.Get2DArray(Name, inp.Name, data, DataTypeHelper.GetDataTypefor2DArrayElements(veeDataType)));
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Coord))
                    {
                        specialInp.CreateCoord1DArray(InputOutputArgumentHelper.GetColumnLength(inp.Name, Name, data), InputOutputArgumentHelper.CreateCoordArray(inp.Name, Name, data));
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Waveform))
                    {
                        specialInp.CreateWaveform(0, 0.02, VeeMapType.veeMapLinear, data);
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Spectrum))
                    {
                        specialInp.CreateSpectrum(0, 1000, VeeMapType.veeMapLinear, InputOutputArgumentHelper.Get2DArray(Name, inp.Name, data, DataTypeHelper.GetDataTypefor2DArrayElements(veeDataType)));
                        inputList.Add(specialInp);
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Text))
                    {
                        InputOutputArgumentHelper.CheckInputFormatFor1DArray(inp.Name, Name, data);
                        inputList.Add(data.Split(','));
                    }
                    else if (veeDataType.Equals(OpenTapDataType.Boolean))
                    {
                        InputOutputArgumentHelper.CheckInputFormatFor1DArray(inp.Name, Name, data);
                        inputList.Add(Array.ConvertAll(data.Split(','), new Converter<string, bool>(bool.Parse)));
                    }
                    else
                    {
                        inputList.Add(data);
                    }
                }
                else if (veeDataShape.Equals(OpenTapDataShape.Array2D))
                {
                    inputList.Add(InputOutputArgumentHelper.Get2DArray(Name, inp.Name, data, DataTypeHelper.GetDataTypefor2DArrayElements(veeDataType)));
                }
                else
                {
                    throw new Exception($"{Name}.{inp.Name}: Input argument support error.");
                }
            }
            return inputList;
        }

        private void getOutputArgumentsAndPublish(VeeDescriptor desc, object outputs)
        {
            int outputIndex = 0;
            foreach (var output in desc.Arguments.Where(x => !x.Input).OrderBy(y => y.Index))
            {
                dynamic temp = ((object[])outputs)[outputIndex];

                if (temp == null)
                {
                    throw new Exception($"{Name}.{output.Name}: Failed to get the output value. Please check the input(s) and try again.");
                }

                object outValue = 0;
                object resultListenerPublishOutput = 0;
                var veeDataType = getDataType(output.Name, output.DataType);
                var veeDataShape = getDataShape(output.Name, output.DataShape, DataTypeHelper.GetShapeCase(veeDataType));

                DataTypeHelper.GetValueTypeAndShape(output.Name, Name, temp, out OpenTapDataType outputValueDataType, out OpenTapDataShape outputValueDataShape);

                if (veeDataType != outputValueDataType)
                {
                    VeeLog.Warning($"{Name}.{output.Name}: Please select the correct data type. Selected data type: {veeDataType}. Actual data type: {outputValueDataType}.");
                }

                if (veeDataShape != outputValueDataShape)
                {
                    VeeLog.Warning($"{Name}.{output.Name}: Please select the correct data shape. Selected data shape: {veeDataShape}. Actual data shape: {outputValueDataShape}.");
                }

                // outputValue has to be converted to the correct type
                if (temp.GetType().FullName.Equals("System.__ComObject"))
                {
                    VeeDataContainer outputValue = (VeeDataContainer)((object[])outputs)[outputIndex];
                    object start = 0;
                    object end = 0;
                    object mapType = 0;
                    object data2DArray = new double[0, 0] as object;
                    object index1Value = 0;
                    object index2Value = 0;
                    object coordDim = 0;
                    object data = new double[0] as object;

                    if (outputValueDataShape.Equals(OpenTapDataShape.Scalar))
                    {
                        if (outputValueDataType.Equals(OpenTapDataType.PComplex))
                        {
                            outputValue.GetPolarComplexScalar(ref index1Value, ref index2Value);
                            double[] complexNumber = new double[] { (double)index1Value, (double)index2Value };
                            InputOutputArgumentHelper.SetOutputValues(complexNumber, complexNumber, out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Complex))
                        {
                            outputValue.GetComplexScalar(ref index1Value, ref index2Value);
                            double[] complexNumber = new double[] { (double)index1Value, (double)index2Value };
                            InputOutputArgumentHelper.SetOutputValues(complexNumber, complexNumber, out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Coord))
                        {
                            outputValue.GetCoordScalar(ref data);
                            InputOutputArgumentHelper.SetOutputValues(data, data, out resultListenerPublishOutput, out outValue);
                        }
                        else
                        {
                            throw new Exception($"{Name}.{output.Name}: Output datatype mismatch/not supported error.");
                        }
                    }
                    else if (outputValueDataShape.Equals(OpenTapDataShape.Array1D))
                    {
                        if (outputValueDataType.Equals(OpenTapDataType.PComplex))
                        {
                            outputValue.GetPolarComplex1DArray(ref data2DArray);
                            InputOutputArgumentHelper.SetOutputValues(data2DArray, InputOutputArgumentHelper.GetDisplay2DArray(data2DArray), out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Complex))
                        {
                            outputValue.GetComplex1DArray(ref data2DArray);
                            InputOutputArgumentHelper.SetOutputValues(data2DArray, InputOutputArgumentHelper.GetDisplay2DArray(data2DArray), out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Spectrum))
                        {
                            outputValue.GetSpectrum(ref start, ref end, ref mapType, ref data2DArray);
                            InputOutputArgumentHelper.SetOutputValues(data2DArray, InputOutputArgumentHelper.GetDisplay2DArray(data2DArray), out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Waveform))
                        {
                            outputValue.GetWaveform(ref start, ref end, ref mapType, ref data);
                            InputOutputArgumentHelper.SetOutputValues(data, data, out resultListenerPublishOutput, out outValue);
                        }
                        else if (outputValueDataType.Equals(OpenTapDataType.Coord))
                        {
                            outputValue.GetCoord1DArray(ref coordDim, ref data);
                            InputOutputArgumentHelper.SetOutputValues(InputOutputArgumentHelper.Get2DArray(Type.VeeDescriptor.FunctionName, output.Name, data, typeof(double), Convert.ToInt32(coordDim)), InputOutputArgumentHelper.GetCoordDisplay1DArray(coordDim, data), out resultListenerPublishOutput, out outValue);
                        }
                    }
                    else
                    {
                        throw new Exception($"{Name}.{output.Name}: Output datatype mismatch/not supported error.");
                    }
                }
                else
                {
                    if (outputValueDataShape.Equals(OpenTapDataShape.Array2D))
                        InputOutputArgumentHelper.SetOutputValues(temp, InputOutputArgumentHelper.GetDisplay2DArray(temp), out resultListenerPublishOutput, out outValue);
                    else if (outputValueDataShape.Equals(OpenTapDataShape.Array1D) && (outputValueDataType.Equals(OpenTapDataType.Text) || outputValueDataType.Equals(OpenTapDataType.Boolean)))
                    {
                        InputOutputArgumentHelper.SetOutputValues(temp, String.Join(",", temp), out resultListenerPublishOutput, out outValue);
                    }
                    else
                        InputOutputArgumentHelper.SetOutputValues(temp, temp, out resultListenerPublishOutput, out outValue);
                }

                string propertyName = $"{output.Name}{DataTypeHelper.GetMatchingTypePair(outputValueDataType, outputValueDataShape).Key?.Name}";

                Type.GetMember(propertyName)?.SetValue(this, outValue);


                try
                {
                    if (OutputCheckType != OutputCheckType.Off && (output.Name == OutputToBeChecked))
                    {
                        bool checkResult = outputCheck(resultListenerPublishOutput);
                        UpgradeVerdict(checkResult ? Verdict.Pass : Verdict.Fail);
                    }
                    if (OutputToResult)
                    {
                        publishVEEResult(desc.FunctionName, output, resultListenerPublishOutput);
                    }
                }
                catch (OutputCheckException ex)
                {
                    VeeLog.Error($"{Name}.{output.Name}: {ex.Message}");
                }
                OnPropertyChanged(propertyName);
                outputIndex++;
            }
        }

        private OpenTapDataType getDataType(string variableName, VeeDataType veeDataType)
        {
            if (veeDataType.Equals(VeeDataType.veeTypeAny) || veeDataType.Equals(VeeDataType.veeTypeNil))
                return (OpenTapDataType)Type.GetMember($"{variableName}{Naming.DataType}").GetValue(this);
            else
                return DataTypeHelper.GetOpenTapDataType(veeDataType);
        }

        private OpenTapDataShape getDataShape(string variableName, VeeDataShape veeDataShape, string shapeCase)
        {
            if (veeDataShape.Equals(VeeDataShape.veeShapeAny))
                return (OpenTapDataShape)Type.GetMember($"{variableName}{Naming.DataShape}{shapeCase}").GetValue(this);
            else
                return DataTypeHelper.GetOpenTapDataShape(veeDataShape);
        }

        private void setDataShapeValuesForAny(IEnumerable<VeeAbstractMember> shapeSelectorList)
        {
            foreach (var shapeSelector in shapeSelectorList)
            {
                if (shapeSelector.Name.Contains("Case1"))
                    shapeSelector.SetValue(this, AvailableDataShapesCase1.FirstOrDefault());
                else if (shapeSelector.Name.Contains("Case2"))
                    shapeSelector.SetValue(this, AvailableDataShapesCase2.FirstOrDefault());
                else
                    shapeSelector.SetValue(this, AvailableDataShapesCase3.FirstOrDefault());
            }
        }

        #endregion

        #region Publish_Result_Functions
        // this method could only be called after output checking
        private void publishVEEResult(string stepName, VeeArgument data, object value)
        {
            var columnNames = new List<string> { data?.Name };
            ArrayList columnValues = new ArrayList();
            bool isValueArray = false;
            int valueArrayLength = 0;

            void addResultColumn(string propName)
            {
                var property = GetType().GetProperty(propName);
                var propValue = property?.GetValue(this);

                if (property == null || propValue == null)
                    return;

                var propValueType = propValue.GetType();

                var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name ?? propName;

                columnNames.Add(displayName);

                if (propValueType.IsEnum)
                    propValue = EnumHelper.GetDisplayName((Enum)propValue);

                if (isValueArray)
                {
                    var propArray = Array.CreateInstance(propValueType.IsEnum ? typeof(string) : propValueType, valueArrayLength);
                    for (int i = 0; i < valueArrayLength; i++)
                    {
                        propArray.SetValue(propValue, i);
                    }
                    columnValues.Add(propArray);
                }
                else
                {
                    columnValues.Add((IConvertible)propValue);
                }
            }

            void publishOutputCheckData()
            {
                addResultColumn(nameof(Verdict));

                if (OutputCheckType == OutputCheckType.Off)
                    return;

                addResultColumn(nameof(OutputCheckType));

                switch (OutputCheckType)
                {
                    case OutputCheckType.StringCheck:
                        addResultColumn(nameof(CheckStringOperator));
                        addResultColumn(nameof(Keyword));
                        break;
                    case OutputCheckType.LimitCheck:
                        addResultColumn(nameof(SelectedLimitType));
                        switch (SelectedLimitType)
                        {
                            case LimitType.Limit:
                                addResultColumn(nameof(SingleOperator));
                                addResultColumn(nameof(SingleLimit));
                                break;
                            case LimitType.Range:
                                addResultColumn(nameof(UpperRangeOperator));
                                addResultColumn(nameof(UpperRangeLimit));
                                addResultColumn(nameof(LowerRangeOperator));
                                addResultColumn(nameof(LowerRangeLimit));
                                break;
                            case LimitType.Tolerance:
                                addResultColumn(nameof(NominalValue));
                                addResultColumn(nameof(PlusTolerance));
                                addResultColumn(nameof(MinusTolerance));
                                break;
                            case LimitType.TolerancePercentage:
                                addResultColumn(nameof(NominalValue));
                                addResultColumn(nameof(PlusTolerancePercent));
                                addResultColumn(nameof(MinusTolerancePercent));
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            try
            {
                string publishName = stepName + "-" + data.Name;
                string valueTypeName = value.GetType().FullName;

                if (valueTypeName.Contains("[,,]") || valueTypeName.Equals("System.__ComObject"))
                {
                    throw new Exception($"{stepName}.{data.Name}: Output datatype is not supported in OpenTAP.");
                }
                else if (valueTypeName.Contains("[]") || valueTypeName.Contains("[,]"))
                {
                    isValueArray = true;
                    dynamic valueArray = value;

                    if (valueTypeName.Contains("[,]"))
                    {
                        // 2d array
                        valueArrayLength = valueArray.GetLength(0);
                        columnNames = getColumnList(data.Name, valueArray.GetLength(1));
                        columnValues.AddRange(get2DArray(valueArray));
                    }
                    else
                    {
                        // 1d array
                        valueArrayLength = valueArray.Length;
                        columnValues.Add(valueArray);
                    }
                    publishOutputCheckData();
                    Results.PublishTable(publishName, columnNames, (Array[])columnValues.ToArray(typeof(Array)));
                }
                else
                {
                    //Scalar Types
                    columnValues.Add((IConvertible)value);
                    publishOutputCheckData();
                    Results.Publish(publishName, columnNames, (IConvertible[])columnValues.ToArray(typeof(IConvertible)));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error in publishing to result listener with message : " + e);
            }
        }

        private List<string> getColumnList(string resultName, int numberOfResultColumns)
        {
            List<string> tables = new List<string>();
            for (int i = 0; i < numberOfResultColumns; i++)
            {
                tables.Add(resultName + ": Column " + i);
            }

            return tables;
        }

        private Array[] get2DArray<T>(T[,] matrix)
        {
            Array[] arrayList = new Array[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                T[] temp = new T[matrix.GetLength(0)];
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    temp[j] = matrix[j, i];
                }
                arrayList[i] = temp;
            }
            return arrayList;
        }

        #endregion
    }

    class OutputCheckException : Exception
    {
        public OutputCheckException(string message) : base(message) { }
    }
}
